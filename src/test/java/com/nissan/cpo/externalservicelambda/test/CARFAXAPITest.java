package com.nissan.cpo.externalservicelambda.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.nissan.cpo.externalservicelambda.handler.ExternalServicesSpringBootApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExternalServicesSpringBootApplication.class)
public class CARFAXAPITest {
	
	
	@Autowired
	WebTestClient webTestClient;
	
	@Autowired
    ApplicationContext context;
		
	@Test
	public void getCARFAXTestSuccess() {
		String vin = "3N1CP5DV9LL513317"; 
		webTestClient.get().uri("/cpo/external/carfax/"+vin)
		.headers(headers -> {
			headers.add("countryCode", "US");
			headers.add("languageCode", "en");
			headers.add("version", "1.0");
			headers.add("division", "Nissan");
		})
		.exchange()
		.expectStatus().isOk();	
	}
	
	@Test
	public void getCARFAXTestInvalidRequest() {
		String vin = "3N1CP5DV9LL513318"; 
		webTestClient.get().uri("/cpo/external/carfax/"+vin)
		.headers(headers -> {
			headers.add("countryCode", "");
			headers.add("languageCode", "en");
			headers.add("version", "1.0");
			headers.add("division", "Nissan");
		})
		.exchange()
		.expectStatus().isBadRequest();
	}
	
	@Test
	public void getCARFAXTestDataNotFound() {
		String vin = "3N1CP5DV9LL513318"; 
		webTestClient.get().uri("/cpo/external/carfax1/"+vin)
		.headers(headers -> {
			headers.add("countryCode", "US");
			headers.add("languageCode", "en");
			headers.add("version", "1.0");
			headers.add("division", "Nissan");
		})
		.exchange()
		.expectStatus().isNotFound();
	}
	
	//@Test
	public void getCARFAXTestInternalServerError() {
		String vin = "3N1CP5DV9LL513318333"; 
		webTestClient.get().uri("/cpo/external/carfax/"+vin)
		.headers(headers -> {
			headers.add("countryCode", "US");
			headers.add("languageCode", "en");
			headers.add("version", "1.0");
			headers.add("division", "Nissan");
		})
		.exchange()
		.expectStatus().is5xxServerError();
	} 

	
}
