package com.nissan.cpo.externalservicelambda.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.nissan.cpo.externalservicelambda.handler.ExternalServicesSpringBootApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExternalServicesSpringBootApplication.class)
public class VinLookupDetailsAPITest {
	
	@Autowired
	WebTestClient webTestClient;
	
	@Autowired
    ApplicationContext context;
	
	@Test
	public void vindetailsTestSuccess() {
		String vin = "1N4AA5AP7EC911027";
		
		webTestClient.get().uri("/cpo/external/vindetails/"+vin)
		.exchange()
		.expectStatus().isOk();
		
	}
	
	@Test
	public void vindetailsTestInvalidRequest() {
		String vin = "1N4AA5AP7EC91102";
		
		webTestClient.get().uri("/cpo/external/vindetails/"+vin)
		.exchange()
		.expectStatus().isBadRequest();
		
	}
	
	@Test
	public void vindetailsTestDataNotFound() {
		String vin = "3N1CP5DV9LL513318";
		
		webTestClient.get().uri("/cpo/external/vindetails/"+vin)
		.exchange()
		.expectStatus().isNotFound();
		
	}

}
