package com.nissan.cpo.externalservicelambda.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;


import com.nissan.cpo.externalservicelambda.handler.ExternalServicesSpringBootApplication;
import com.nissan.cpo.externalservicelambda.model.ServiceHistoryRequestModel;
import com.nissan.cpo.externalservicelambda.model.VehicleServiceHistory;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExternalServicesSpringBootApplication.class)
public class ServiceHistoryAPITest {

	@Autowired
	WebTestClient webTestClient;
	
	@Autowired
    ApplicationContext context;
	
	VehicleServiceHistory vehicleServiceHistory = new VehicleServiceHistory();
	
	@Test
	public void serviceHistoryTestSuccess() {
		ServiceHistoryRequestModel serviceHistoryRequestModel = new 
				ServiceHistoryRequestModel("DWAHIZ39", "D", "5309","JN1BJ0RR5GM262027", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/servicehistory")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(serviceHistoryRequestModel), ServiceHistoryRequestModel.class)
		.exchange()
		.expectStatus().isOk();
	}
	
	@Test
	public void serviceHistoryTestInvalidRequest() {
		
		ServiceHistoryRequestModel serviceHistoryRequestModel = new 
				ServiceHistoryRequestModel("DWAHIZ39", "D", "","JN1BJ0RR5GM262027", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/servicehistory")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(serviceHistoryRequestModel), ServiceHistoryRequestModel.class)
		.exchange()
		.expectStatus().isBadRequest();
			
	}
	
	@Test
	public void serviceHistoryTestDataNotFound() {
		
		ServiceHistoryRequestModel serviceHistoryRequestModel = new 
				ServiceHistoryRequestModel("DWAHIZ39", "D", "5309","JN1BJ0RR5GM26202", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/servicehistory")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(serviceHistoryRequestModel), ServiceHistoryRequestModel.class)
		.exchange()
		.expectStatus().isNotFound();
			
	}
	
}
