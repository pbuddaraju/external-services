package com.nissan.cpo.externalservicelambda.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.nissan.cpo.externalservicelambda.handler.ExternalServicesSpringBootApplication;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsRequestModel;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsResponseModel;

import reactor.core.publisher.Mono;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ExternalServicesSpringBootApplication.class)
public class GetDealerDetailsAPITest {
	
	
	@Autowired
	WebTestClient webTestClient;
	
	@Autowired
    ApplicationContext context;
	
	GetDealerDetailsResponseModel getDealerDetailsResponseModel = new GetDealerDetailsResponseModel();
	
	@Test
	public void getDealerDetailsTestSuccess() {
		
		GetDealerDetailsRequestModel getDealerDetailsRequestModel = new 
				GetDealerDetailsRequestModel("US", "en-us", "NI", "DWAHIZ39", "D", "", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/getDealerDetails")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(getDealerDetailsRequestModel), GetDealerDetailsRequestModel.class)
		.exchange()
		.expectStatus().isOk();
			
	}
	
	@Test
	public void getDealerDetailsTestInvalidRequest() {
		
		GetDealerDetailsRequestModel getDealerDetailsRequestModel = new 
				GetDealerDetailsRequestModel("US", "en-us", "NI", "DWAHIZ39", "", "", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/getDealerDetails")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(getDealerDetailsRequestModel), GetDealerDetailsRequestModel.class)
		.exchange()
		.expectStatus().isBadRequest();
			
	}
	
	@Test
	public void getDealerDetailsTestDataNotFound() {
		
		GetDealerDetailsRequestModel getDealerDetailsRequestModel = new 
				GetDealerDetailsRequestModel("US", "en-us", "NI", "DNCARP80", "D", "", "CPO", "1.0.0 STG", "desktop", "dev");
		
		webTestClient.post().uri("/cpo/external/getDealerDetails")
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(getDealerDetailsRequestModel), GetDealerDetailsRequestModel.class)
		.exchange()
		.expectStatus().isNotFound();
			
	}
	
}
