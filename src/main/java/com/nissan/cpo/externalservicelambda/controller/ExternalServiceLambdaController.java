package com.nissan.cpo.externalservicelambda.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nissan.cpo.externalservicelambda.common.CPOConstants;
import com.nissan.cpo.externalservicelambda.common.CPOErrorConstants;
import com.nissan.cpo.externalservicelambda.common.ErrorResponseModel;
import com.nissan.cpo.externalservicelambda.exception.DataNotFoundException;
import com.nissan.cpo.externalservicelambda.exception.InvalidRequestException;
import com.nissan.cpo.externalservicelambda.model.CARFAXResponseModel;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsRequestModel;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsResponseModel;
import com.nissan.cpo.externalservicelambda.model.ServiceHistoryRequestModel;
import com.nissan.cpo.externalservicelambda.model.VehicleServiceHistory;
import com.nissan.cpo.externalservicelambda.model.VinDetailsResponse;
import com.nissan.cpo.externalservicelambda.services.ExternalServiceLambdaService;
import com.nissan.cpo.externalservicelambda.soapclient.SoapServiceHistoryClient;
import com.nissan.cpo.externalservicelambda.soapclient.VinlookupSoapClient;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

@RestController
@RequestMapping("/cpo/external")
@Tag(name = "external", description = "External Services API")
public class ExternalServiceLambdaController {
	
	Logger log = LoggerFactory.getLogger(ExternalServiceLambdaController.class);

	@Autowired
	private ExternalServiceLambdaService externalServiceLambdaService;
	
	@Autowired
	private SoapServiceHistoryClient soapServiceHistoryClient;
	
	@Autowired
	private VinlookupSoapClient vinlookupSoapClient;
	
	private static final String ROOT_CONTEXT = "/cpo/external";

	@Operation(summary = "Fetch DealerList", description = "Endpoint retrieves the dealership list linked with the UserId", tags = "external")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "200",
							description = "Successful operation",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = GetDealerDetailsResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "400",
							description = "Invalid request error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "404",
							description = "Valid data not found error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							)
			}
			)
	@PostMapping("/getDealerDetails")
	public ResponseEntity<Object> getDealerDetails(@RequestBody GetDealerDetailsRequestModel getDealerDetailsRequest) throws Exception {
		GetDealerDetailsResponseModel getDealerDetailsResponse = new GetDealerDetailsResponseModel();
		String requestUri = ROOT_CONTEXT + "/getDealerDetails";
		try {

			if(getDealerDetailsRequest.getApplicationName() == null || getDealerDetailsRequest.getApplicationName().isEmpty() 
					|| !getDealerDetailsRequest.getApplicationName().equalsIgnoreCase("CPO")) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_APPLICATION_NAME, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_APPLICATION_NAME, requestUri);
			} else if(getDealerDetailsRequest.getUserId() == null || getDealerDetailsRequest.getUserId().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_USERID, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_USERID, requestUri);
			} else if(getDealerDetailsRequest.getCountryCode() == null || getDealerDetailsRequest.getCountryCode().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_COUNTRYCODE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_COUNTRYCODE, requestUri);
			} else if(getDealerDetailsRequest.getLanguageCode() == null || getDealerDetailsRequest.getLanguageCode().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_LANGUAGECODE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_LANGUAGECODE, requestUri);
			} else if(getDealerDetailsRequest.getDivision() == null || getDealerDetailsRequest.getDivision().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_DIVISION, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_DIVISION, requestUri);
			} else if(getDealerDetailsRequest.getUserType() == null || getDealerDetailsRequest.getUserType().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_USERTYPE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_USERTYPE, requestUri);
			} else {

				getDealerDetailsResponse = externalServiceLambdaService.getDealerDetailsResponse(getDealerDetailsRequest);

				if(getDealerDetailsResponse.getStatus().equalsIgnoreCase(CPOConstants.GNRL_ERR)) {
					throw new Exception(CPOErrorConstants.CPO_ERR_MESSAGE_GNRL_ERR);
				} else if(getDealerDetailsResponse.getStatus().equalsIgnoreCase(CPOConstants.NO_ACTIVE_DEALERS)) {
					throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_NO_ACTIVE_DEALERS, 
							CPOErrorConstants.CPO_ERR_MESSAGE_NO_ACTIVE_DEALERS, requestUri);
				} else if(getDealerDetailsResponse.getStatus().equalsIgnoreCase(CPOConstants.NO_BRAND_DEALERS)) {
					throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_NO_BRAND_DEALERS, 
							CPOErrorConstants.CPO_ERR_MESSAGE_NO_BRAND_DEALERS, requestUri);
				} else if(getDealerDetailsResponse.getStatus().equalsIgnoreCase(CPOConstants.NO_ACTIVE_OR_BRAND_DEALERS)) {
					throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_NO_ACTIVE_OR_BRAND_DEALERS, 
							CPOErrorConstants.CPO_ERR_MESSAGE_NO_ACTIVE_OR_BRAND_DEALERS, requestUri);
				} else {
					return new ResponseEntity<>(getDealerDetailsResponse, HttpStatus.OK);
				}
			}

		} catch (DataNotFoundException e) {
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		}catch (InvalidRequestException e) {
			throw new InvalidRequestException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (Exception e) {
			throw new Exception(e.getMessage()); 
		}

	}
	
	@Operation(summary = "To check CARFAX Status", description = "API will connect the CARFAX service and return Pass/Fail Status", tags = {"CARFAX"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully received CARFAX Vin Pass/Fail Status", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CARFAXResponseModel.class)))),
			@ApiResponse(responseCode = "400", description = "Bad input data", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ErrorResponseModel.class)))),
			@ApiResponse(responseCode = "500", description = "Internal Server error", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ErrorResponseModel.class)))),
	})
	@GetMapping("/carfax/{vin}")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<Object> getCARFAXVinStatusInfo (
    		@Parameter(example = "US") @RequestHeader(name = "countryCode", required = true) String countryCode,
    		@Parameter(example = "en") @RequestHeader(name = "languageCode", required = true) String languageCode,
    		@Parameter(example = "1.0") @RequestHeader(name = "version", required = true) String apiVersion,
    		@Parameter(example = "Nissan") @RequestHeader(name = "division", required = true) String division,
    		@Parameter(example="3N1CP5DV8LL494713") @PathVariable("vin") String vin
    		) {
		CARFAXResponseModel response = new CARFAXResponseModel();
		try { 
			if(countryCode== null || countryCode.isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_COUNTRYCODE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_COUNTRYCODE, "getCARFAXVinStatusInfo");
				
			} else if(languageCode == null || languageCode.isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_LANGUAGECODE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_LANGUAGECODE, "getCARFAXVinStatusInfo");
			} else if(division == null || division.isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_DIVISION, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_DIVISION, "getCARFAXVinStatusInfo");
			} 
			
			response = externalServiceLambdaService.getCARFAXResponse(countryCode, languageCode, vin);
			
			if(response != null & response.getStatus().equalsIgnoreCase(CPOConstants.FAILURE)) {
				return new ResponseEntity<>(response.getStatusDesc(), HttpStatus.INTERNAL_SERVER_ERROR);
			} else if(response != null & response.getStatusDesc().contains("600")) {
				throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_NO_DATA_AVAILABLE, 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("800")) {
				throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_INVALID_VIN, 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("900")) {
				throw new DataNotFoundException(HttpStatus.SERVICE_UNAVAILABLE.toString(), 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("901")) {
				throw new DataNotFoundException(HttpStatus.INTERNAL_SERVER_ERROR.toString(), 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("903")) {
				throw new DataNotFoundException(HttpStatus.INTERNAL_SERVER_ERROR.toString(), 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("904")) {
				throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_INVALID_CARFAX_IP_ADDRESS, 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			} else if(response != null & response.getStatusDesc().contains("905")) {
				throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_INVALID_USERID, 
						response.getStatusDesc(), "getCARFAXVinStatusInfo");
			}
		} catch (DataNotFoundException e) {
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		}catch (InvalidRequestException e) {
			throw new InvalidRequestException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (Exception e) { 
		  return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	@Operation(summary = "Fetch ServiceHistory", description = "Endpoint retrieves the Service History details list linked with the VIN and DealerCode", tags = "external")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "200",
							description = "Successful operation",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = VehicleServiceHistory.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "400",
							description = "Invalid request error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "404",
							description = "Valid data not found error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							)
			}
			)
	@PostMapping("/servicehistory")
	public ResponseEntity<Object> fetchServiceHistory(
			@RequestHeader(name = "countryCode", defaultValue = "US") String countryCode,
			@RequestHeader(name = "languageCode", defaultValue = "en-us") String languageCode,
			@RequestHeader(name = "division", defaultValue = "NI") String division,
			@RequestBody ServiceHistoryRequestModel serviceHistoryRequestModel) throws Exception {
		String requestUri = ROOT_CONTEXT + "/servicehistory";
		VehicleServiceHistory vehicleServiceHistory = new VehicleServiceHistory();
		try {
			
			if(serviceHistoryRequestModel.getDealerCode() == null || serviceHistoryRequestModel.getDealerCode().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_DEALERCODE, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_DEALERCODE, requestUri);
			} else if(serviceHistoryRequestModel.getApplicationName() == null || serviceHistoryRequestModel.getApplicationName().isEmpty()
					|| !serviceHistoryRequestModel.getApplicationName().equalsIgnoreCase("CPO")) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_APPLICATION_NAME, 
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_APPLICATION_NAME, requestUri);
			} else if(serviceHistoryRequestModel.getVin() == null || serviceHistoryRequestModel.getVin().isEmpty()) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_VIN,
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_VIN, requestUri);
			} else {
				vehicleServiceHistory = soapServiceHistoryClient.getServiceHistory(countryCode, division, 
						serviceHistoryRequestModel.getVin(), serviceHistoryRequestModel.getDealerCode());
				if(vehicleServiceHistory.getStatus().equalsIgnoreCase(CPOConstants.GNRL_ERR)) {
					throw new Exception(CPOErrorConstants.CPO_ERR_MESSAGE_GNRL_ERR);
				}else if(vehicleServiceHistory.getStatus().equalsIgnoreCase(CPOConstants.FAILURE)) {
					throw new Exception(CPOErrorConstants.CPO_ERR_MESSAGE_GNRL_ERR);
				} else {
					return new ResponseEntity<>(vehicleServiceHistory, HttpStatus.OK);
				}
			}
			
		} catch (DataNotFoundException e) {
			log.error("ExternalServiceLambdaController class - fetchServiceHistory() method - inside DataNotFoundException" + e.getMessage());
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (InvalidRequestException e) {
			log.error("ExternalServiceLambdaController class - fetchServiceHistory() method - inside InvalidRequestException" + e.getMessage());
			throw new InvalidRequestException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (Exception e) {
			log.error("ExternalServiceLambdaController class - fetchServiceHistory() method - inside General Exception" + e.getMessage());
			throw new Exception(e.getMessage()); 
		}
		
	}
	
	@Operation(summary = "Fetch VinlookupDetails", description = "Endpoint retrieves the Vinlookup details linked with the VIN from BIDW", tags = "external")
	@ApiResponses(
			value = {
					@ApiResponse(
							responseCode = "200",
							description = "Successful operation",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = VinDetailsResponse.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "400",
							description = "Invalid request error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "404",
							description = "Valid data not found error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							),
					@ApiResponse(
							responseCode = "500",
							description = "Internal Server error",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(implementation = ErrorResponseModel.class)
											)
									)
							)
			}
			)
	@GetMapping("/vindetails/{vin}")
	public ResponseEntity<Object> getVinDetails(@RequestHeader(name = "countryCode", defaultValue = "US") String countryCode,
			@RequestHeader(name = "languageCode", defaultValue = "en-us") String languageCode,
			@RequestHeader(name = "division", defaultValue = "NI") String division,
			@RequestHeader(name = "appUserName", defaultValue = "CPO") String appUserName,
			@PathVariable String vin) throws Exception {
		String requestUri = ROOT_CONTEXT + "/vindetails";
		VinDetailsResponse vinDetailsResponse = new VinDetailsResponse();
		try {

			if(vin == null || vin.isEmpty() || vin.length() < 17) {
				throw new InvalidRequestException(CPOErrorConstants.CPO_ERR_CODE_INVALID_VIN,
						CPOErrorConstants.CPO_ERR_MESSAGE_INVALID_VIN, requestUri);
			} else {
				
				vinDetailsResponse = vinlookupSoapClient.getVinlookupDetails(vin, division, countryCode, requestUri);
				if(vinDetailsResponse.getStatus().equalsIgnoreCase(CPOConstants.FAILURE)) {
					throw new Exception(CPOErrorConstants.CPO_ERR_MESSAGE_GNRL_ERR);
				} else {
					return new ResponseEntity<>(vinDetailsResponse, HttpStatus.OK);
				}
			}

		} catch (DataNotFoundException e) {
			log.error("ExternalServiceLambdaController class - getVinDetails() method - inside DataNotFoundException" + e.getMessage());
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (InvalidRequestException e) {
			log.error("ExternalServiceLambdaController class - getVinDetails() method - inside InvalidRequestException" + e.getMessage());
			throw new InvalidRequestException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ExternalServiceLambdaController class - getVinDetails() method - inside General Exception" + e.getMessage());
			throw new Exception(e.getMessage()); 
		}

	}

}
