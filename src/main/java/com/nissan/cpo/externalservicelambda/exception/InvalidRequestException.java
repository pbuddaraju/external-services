package com.nissan.cpo.externalservicelambda.exception;

public class InvalidRequestException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	private String errorCode;
	private String errorMessage;
	private String requestUri;
	
	public InvalidRequestException(String message) {
		super(message);
	}

	public InvalidRequestException(String errorCode, String errorMessage, String requestUri) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.requestUri = requestUri;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InvalidRequestException [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", requestUri="
				+ requestUri + "]";
	}
	
}
