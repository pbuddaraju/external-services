package com.nissan.cpo.externalservicelambda.exception;


/**
 * @see CPO Exception class which is used to capture all the exception 
 * @author x566325
 * @category Common Exception
 */



public class CPOException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CPOException() {
        super();
    }
	
	public CPOException(String errorMessage) {
        super(errorMessage);
    }
	
	public CPOException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }
	
}
