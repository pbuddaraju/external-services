package com.nissan.cpo.externalservicelambda.exception;

public class DataNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private String errorMessage;
	private String requestUri;

	public DataNotFoundException(String message) {
		super(message);
	}

	public DataNotFoundException(String errorCode, String errorMessage, String requestUri) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.requestUri = requestUri;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	@Override
	public String toString() {
		return "DataNotFoundException [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", requestUri="
				+ requestUri + "]";
	}

}
