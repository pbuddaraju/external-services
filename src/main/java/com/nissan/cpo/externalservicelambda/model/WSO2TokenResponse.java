package com.nissan.cpo.externalservicelambda.model;

public class WSO2TokenResponse {
	
	public String access_token;
    public String scope;
    public String token_type;
    public int expires_in;
    
    public WSO2TokenResponse() {
    	
    }

	public WSO2TokenResponse(String access_token, String scope, String token_type, int expires_in) {
		super();
		this.access_token = access_token;
		this.scope = scope;
		this.token_type = token_type;
		this.expires_in = expires_in;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	@Override
	public String toString() {
		return "WSO2TokenResponse [access_token=" + access_token + ", scope=" + scope + ", token_type=" + token_type
				+ ", expires_in=" + expires_in + "]";
	}

}
