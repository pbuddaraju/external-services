package com.nissan.cpo.externalservicelambda.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class VehicleServiceHistoryDetail {
	@Schema(description = "Value of the repair open date for the service history detail", example = "2018-12-13Z", required = true)
	private String repairOrderOpenedDate;
	@Schema(description = "Value of the repair complete date for the service history detail", example = "2018-12-15Z", required = true)
	private String repairOrderCompletedDate;
	@Schema(description = "List of Job history details for the service history detail", example = "", required = true)
	private List<JobHistory> jobHistory;
	@Schema(description = "Value of the in distance measure for the service history detail", example = "18773.0 miles", required = true)
	private String inDistanceMeasure;
	@Schema(description = "Value of the out distance measure for the service history detail", example = "18773.0 miles", required = true)
	private String outDistanceMeasure;
	
	public VehicleServiceHistoryDetail() {
		
	}

	public VehicleServiceHistoryDetail(String repairOrderOpenedDate, String repairOrderCompletedDate,
			List<JobHistory> jobHistory, String inDistanceMeasure, String outDistanceMeasure) {
		super();
		this.repairOrderOpenedDate = repairOrderOpenedDate;
		this.repairOrderCompletedDate = repairOrderCompletedDate;
		this.jobHistory = jobHistory;
		this.inDistanceMeasure = inDistanceMeasure;
		this.outDistanceMeasure = outDistanceMeasure;
	}

	public String getRepairOrderOpenedDate() {
		return repairOrderOpenedDate;
	}

	public void setRepairOrderOpenedDate(String repairOrderOpenedDate) {
		this.repairOrderOpenedDate = repairOrderOpenedDate;
	}

	public String getRepairOrderCompletedDate() {
		return repairOrderCompletedDate;
	}

	public void setRepairOrderCompletedDate(String repairOrderCompletedDate) {
		this.repairOrderCompletedDate = repairOrderCompletedDate;
	}

	public List<JobHistory> getJobHistory() {
		return jobHistory;
	}

	public void setJobHistory(List<JobHistory> jobHistory) {
		this.jobHistory = jobHistory;
	}

	public String getInDistanceMeasure() {
		return inDistanceMeasure;
	}

	public void setInDistanceMeasure(String inDistanceMeasure) {
		this.inDistanceMeasure = inDistanceMeasure;
	}

	public String getOutDistanceMeasure() {
		return outDistanceMeasure;
	}

	public void setOutDistanceMeasure(String outDistanceMeasure) {
		this.outDistanceMeasure = outDistanceMeasure;
	}

	@Override
	public String toString() {
		return "VehicleServiceHistoryDetail [repairOrderOpenedDate=" + repairOrderOpenedDate
				+ ", repairOrderCompletedDate=" + repairOrderCompletedDate + ", jobHistory=" + jobHistory
				+ ", inDistanceMeasure=" + inDistanceMeasure + ", outDistanceMeasure=" + outDistanceMeasure + "]";
	}

}
