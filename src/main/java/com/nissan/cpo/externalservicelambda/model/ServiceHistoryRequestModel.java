package com.nissan.cpo.externalservicelambda.model;

public class ServiceHistoryRequestModel {
	
	private String userId;
	private String userType;
	private String dealerCode;
	private String vin;
	private String applicationName;
	private String appVersion;
	private String deviceType;
	private String env;
	
	public ServiceHistoryRequestModel() {
		
	}

	public ServiceHistoryRequestModel(String userId, String userType, String dealerCode, String vin,
			String applicationName, String appVersion, String deviceType, String env) {
		super();
		this.userId = userId;
		this.userType = userType;
		this.dealerCode = dealerCode;
		this.vin = vin;
		this.applicationName = applicationName;
		this.appVersion = appVersion;
		this.deviceType = deviceType;
		this.env = env;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	@Override
	public String toString() {
		return "ServiceHistoryRequestModel [userId=" + userId + ", userType=" + userType + ", dealerCode=" + dealerCode
				+ ", vin=" + vin + ", applicationName=" + applicationName + ", appVersion=" + appVersion
				+ ", deviceType=" + deviceType + ", env=" + env + "]";
	}
	
}
