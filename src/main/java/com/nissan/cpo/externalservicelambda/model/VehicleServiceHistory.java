package com.nissan.cpo.externalservicelambda.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class VehicleServiceHistory {
	@Schema(description = "Status of the service", example = "Success", required = true)
	private String status;
	@Schema(description = "Value of the Inservice Date", example = "2015-12-09Z", required = true)
	private String inServiceDate;
	@Schema(description = "Value of the Inservice distance measure in miles", example = "4500.0 miles", required = true)
    private String inServiceDistanceMeasure;
	@Schema(description = "Value of the warranty expiration date", example = "2019-12-09Z", required = true)
    private String warrantyExpirationDate;
	@Schema(description = "Value of the warranty end distance measure in miles", example = "60000.0 miles", required = true)
    private String warrantyEndDistanceMeasure;
	@Schema(description = "Value of the vehicle delivery date", example = "2015-12-09Z", required = true)
    private String deliveryDate;
	@Schema(description = "Value of the vehicle build date", example = "2015-09-24Z", required = true)
    private String vehicleBuildDate;
    private List<VehicleServiceHistoryDetail> vehicleServiceHistoryDetail;
    
    public VehicleServiceHistory() {
    	
    }
    
	public VehicleServiceHistory(String status, String inServiceDate, String inServiceDistanceMeasure, String warrantyExpirationDate,
			String warrantyEndDistanceMeasure, String deliveryDate, String vehicleBuildDate,
			List<VehicleServiceHistoryDetail> vehicleServiceHistoryDetail) {
		super();
		this.status = status;
		this.inServiceDate = inServiceDate;
		this.inServiceDistanceMeasure = inServiceDistanceMeasure;
		this.warrantyExpirationDate = warrantyExpirationDate;
		this.warrantyEndDistanceMeasure = warrantyEndDistanceMeasure;
		this.deliveryDate = deliveryDate;
		this.vehicleBuildDate = vehicleBuildDate;
		this.vehicleServiceHistoryDetail = vehicleServiceHistoryDetail;
	}

	public String getInServiceDate() {
		return inServiceDate;
	}

	public void setInServiceDate(String inServiceDate) {
		this.inServiceDate = inServiceDate;
	}

	public String getInServiceDistanceMeasure() {
		return inServiceDistanceMeasure;
	}

	public void setInServiceDistanceMeasure(String inServiceDistanceMeasure) {
		this.inServiceDistanceMeasure = inServiceDistanceMeasure;
	}

	public String getWarrantyExpirationDate() {
		return warrantyExpirationDate;
	}

	public void setWarrantyExpirationDate(String warrantyExpirationDate) {
		this.warrantyExpirationDate = warrantyExpirationDate;
	}

	public String getWarrantyEndDistanceMeasure() {
		return warrantyEndDistanceMeasure;
	}

	public void setWarrantyEndDistanceMeasure(String warrantyEndDistanceMeasure) {
		this.warrantyEndDistanceMeasure = warrantyEndDistanceMeasure;
	}
	
	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getVehicleBuildDate() {
		return vehicleBuildDate;
	}

	public void setVehicleBuildDate(String vehicleBuildDate) {
		this.vehicleBuildDate = vehicleBuildDate;
	}

	public List<VehicleServiceHistoryDetail> getVehicleServiceHistoryDetail() {
		return vehicleServiceHistoryDetail;
	}

	public void setVehicleServiceHistoryDetail(List<VehicleServiceHistoryDetail> vehicleServiceHistoryDetail) {
		this.vehicleServiceHistoryDetail = vehicleServiceHistoryDetail;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "VehicleServiceHistory [status=" + status + ", inServiceDate=" + inServiceDate
				+ ", inServiceDistanceMeasure=" + inServiceDistanceMeasure + ", warrantyExpirationDate="
				+ warrantyExpirationDate + ", warrantyEndDistanceMeasure=" + warrantyEndDistanceMeasure
				+ ", deliveryDate=" + deliveryDate + ", vehicleBuildDate=" + vehicleBuildDate
				+ ", vehicleServiceHistoryDetail=" + vehicleServiceHistoryDetail + "]";
	}

}
