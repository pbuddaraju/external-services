package com.nissan.cpo.externalservicelambda.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonIgnoreProperties(value = { "dealerAffiliateCode", 
		"dealerAccessLevel", "dealerEmployeeNumber", "crmProvider", "salesPersonCrmId" })
public class DealershipList {
	@Schema(description = "Dealercode selected by the user", example = "5490", required = true)
	public String dealerCode;
	@Schema(description = "Dealership name selected", example = "NISSAN OF SACRAMENTO", required = true)
    public String dealerName;
    public String dealerAffiliateCode;
    public String dealerAccessLevel;
    @Schema(description = "Application roles list mapped for the user", example = "\"roles\": [\r\n" + 
    		"                \"Service_Admin\"\r\n" + 
    		"            ]", required = true)
    public List<String> roles;
    public String dealerEmployeeNumber;
    public String crmProvider;
    public String salesPersonCrmId;
	
	public DealershipList() {
		
	}

	public DealershipList(String dealerCode, String dealerName, String dealerAffiliateCode, String dealerAccessLevel,
			List<String> roles, String dealerEmployeeNumber, String crmProvider, String salesPersonCrmId) {
		super();
		this.dealerCode = dealerCode;
		this.dealerName = dealerName;
		this.dealerAffiliateCode = dealerAffiliateCode;
		this.dealerAccessLevel = dealerAccessLevel;
		this.roles = roles;
		this.dealerEmployeeNumber = dealerEmployeeNumber;
		this.crmProvider = crmProvider;
		this.salesPersonCrmId = salesPersonCrmId;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerAffiliateCode() {
		return dealerAffiliateCode;
	}

	public void setDealerAffiliateCode(String dealerAffiliateCode) {
		this.dealerAffiliateCode = dealerAffiliateCode;
	}

	public String getDealerAccessLevel() {
		return dealerAccessLevel;
	}

	public void setDealerAccessLevel(String dealerAccessLevel) {
		this.dealerAccessLevel = dealerAccessLevel;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getDealerEmployeeNumber() {
		return dealerEmployeeNumber;
	}

	public void setDealerEmployeeNumber(String dealerEmployeeNumber) {
		this.dealerEmployeeNumber = dealerEmployeeNumber;
	}

	public String getCrmProvider() {
		return crmProvider;
	}

	public void setCrmProvider(String crmProvider) {
		this.crmProvider = crmProvider;
	}

	public String getSalesPersonCrmId() {
		return salesPersonCrmId;
	}

	public void setSalesPersonCrmId(String salesPersonCrmId) {
		this.salesPersonCrmId = salesPersonCrmId;
	}

	@Override
	public String toString() {
		return "DealershipList [dealerCode=" + dealerCode + ", dealerName=" + dealerName + ", dealerAffiliateCode="
				+ dealerAffiliateCode + ", dealerAccessLevel=" + dealerAccessLevel + ", roles=" + roles
				+ ", dealerEmployeeNumber=" + dealerEmployeeNumber + ", crmProvider=" + crmProvider
				+ ", salesPersonCrmId=" + salesPersonCrmId + "]";
	}

	
	    
}
