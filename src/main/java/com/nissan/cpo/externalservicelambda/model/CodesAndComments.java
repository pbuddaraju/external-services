package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class CodesAndComments {
	@Schema(description = "Value of the technicianNotes for the CodesAndComments", example = "")
	private String technicianNotes;
	@Schema(description = "Value of the correctionDescription for the CodesAndComments", example = "")
    private String correctionDescription;
	@Schema(description = "Value of the miscellaneousNotes for the CodesAndComments", example = "")
    private String miscellaneousNotes;
	@Schema(description = "Value of the complaintDescription for the CodesAndComments", example = "")
    private String complaintDescription;
	@Schema(description = "Value of the jobDenialDescription for the CodesAndComments", example = "*Basic Package   *Change Engine Oil  Filter Conventional Oil Up to 6 Quarts")
    private String jobDenialDescription;
	@Schema(description = "Value of the causeDescription for the CodesAndComments", example = "")
    private String causeDescription;
    
    public CodesAndComments() {
    	
    }
    
    public CodesAndComments(String technicianNotes, String correctionDescription, String miscellaneousNotes,
			String complaintDescription, String jobDenialDescription, String causeDescription) {
		super();
		this.technicianNotes = technicianNotes;
		this.correctionDescription = correctionDescription;
		this.miscellaneousNotes = miscellaneousNotes;
		this.complaintDescription = complaintDescription;
		this.jobDenialDescription = jobDenialDescription;
		this.causeDescription = causeDescription;
	}

	public String getTechnicianNotes ()
    {
        return technicianNotes;
    }

    public void setTechnicianNotes (String technicianNotes)
    {
        this.technicianNotes = technicianNotes;
    }

    public String getCorrectionDescription ()
    {
        return correctionDescription;
    }

    public void setCorrectionDescription (String correctionDescription)
    {
        this.correctionDescription = correctionDescription;
    }

    public String getMiscellaneousNotes ()
    {
        return miscellaneousNotes;
    }

    public void setMiscellaneousNotes (String miscellaneousNotes)
    {
        this.miscellaneousNotes = miscellaneousNotes;
    }

    public String getComplaintDescription ()
    {
        return complaintDescription;
    }

    public void setComplaintDescription (String complaintDescription)
    {
        this.complaintDescription = complaintDescription;
    }

    public String getJobDenialDescription ()
    {
        return jobDenialDescription;
    }

    public void setJobDenialDescription (String jobDenialDescription)
    {
        this.jobDenialDescription = jobDenialDescription;
    }

    public String getCauseDescription ()
    {
        return causeDescription;
    }

    public void setCauseDescription (String causeDescription)
    {
        this.causeDescription = causeDescription;
    }

	@Override
	public String toString() {
		return "CodesAndComments [technicianNotes=" + technicianNotes + ", correctionDescription="
				+ correctionDescription + ", miscellaneousNotes=" + miscellaneousNotes + ", complaintDescription="
				+ complaintDescription + ", jobDenialDescription=" + jobDenialDescription + ", causeDescription="
				+ causeDescription + "]";
	}

}
