package com.nissan.cpo.externalservicelambda.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetDealerDetailsResponseModel {
	@Schema(description = "Status of the service", example = "Success", required = true)
	private String status;
	@Schema(description = "Country code of the app", example = "US", required = true)
	private String countryCode;
	@Schema(description = "Language value of the app", example = "en-us", required = true)
    private String languageCode;
	@Schema(description = "Username of the requester", example = "DWAHIZ39", required = true)
    private String userId;
	@Schema(description = "Usertype of the requester", example = "D", required = true)
    private String userType;
	@Schema(description = "Brand of the app", example = "NI", required = true)
    private String division;
	@Schema(description = "Firstname of the user", example = "Zoheb", required = true)
    private String firstName;
	@Schema(description = "Lastname of the user", example = "Wahid", required = true)
    private String lastName;
    private List<DealershipList> dealershipList;
    
    public GetDealerDetailsResponseModel() {
    	
    }

	public GetDealerDetailsResponseModel(String status, String countryCode, String languageCode, String userId,
			String userType, String division, String firstName, String lastName, List<DealershipList> dealershipList) {
		super();
		this.status = status;
		this.countryCode = countryCode;
		this.languageCode = languageCode;
		this.userId = userId;
		this.userType = userType;
		this.division = division;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dealershipList = dealershipList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<DealershipList> getDealershipList() {
		return dealershipList;
	}

	public void setDealershipList(List<DealershipList> dealershipList) {
		this.dealershipList = dealershipList;
	}

	@Override
	public String toString() {
		return "GetDealerDetailsResponseModel [status=" + status + ", countryCode=" + countryCode + ", languageCode="
				+ languageCode + ", userId=" + userId + ", userType=" + userType + ", division=" + division
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", dealershipList=" + dealershipList + "]";
	}
    
}
