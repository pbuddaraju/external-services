package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class ServicePartsHistory {
	@Schema(description = "Value of the itemID for the ServicePartsHistory", example = "INF12001200")
	private String itemID;
	@Schema(description = "Value of the itemIdDescription for the ServicePartsHistory", example = "VACUUM TUMBLER")
	private String itemIdDescription;
	@Schema(description = "Value of the itemQuantity for the ServicePartsHistory", example = "1")
	private String itemQuantity;
	
	public ServicePartsHistory() {
		
	}

	public ServicePartsHistory(String itemID, String itemIdDescription, String itemQuantity) {
		super();
		this.itemID = itemID;
		this.itemIdDescription = itemIdDescription;
		this.itemQuantity = itemQuantity;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public String getItemIdDescription() {
		return itemIdDescription;
	}

	public void setItemIdDescription(String itemIdDescription) {
		this.itemIdDescription = itemIdDescription;
	}

	public String getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(String itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	@Override
	public String toString() {
		return "ServicePartsHistory [itemID=" + itemID + ", itemIdDescription=" + itemIdDescription + ", itemQuantity="
				+ itemQuantity + "]";
	}

}
