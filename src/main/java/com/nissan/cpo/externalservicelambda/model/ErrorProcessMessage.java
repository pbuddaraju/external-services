package com.nissan.cpo.externalservicelambda.model;

public class ErrorProcessMessage {
	
	private String description;
	private String type;
	private String reasonCode;
	
	public ErrorProcessMessage() {
		
	}

	public ErrorProcessMessage(String description, String type, String reasonCode) {
		super();
		this.description = description;
		this.type = type;
		this.reasonCode = reasonCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	@Override
	public String toString() {
		return "ErrorProcessMessage [description=" + description + ", type=" + type + ", reasonCode=" + reasonCode
				+ "]";
	}

}
