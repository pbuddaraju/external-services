package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class VinDetailsResponse {
	@Schema(description = "Status of the service", example = "Success", required = true)
	private String status;
	@Schema(description = "Vin passed in the request", example = "3N1CP5DV8LL494713", required = true)
	private String vin;
	@Schema(description = "DrivetrainName of the vehicle", example = "KICKS SR", required = true)
	private String drivetrainName;
	@Schema(description = "TransmissionTypeName of the vehicle", example = "CVT", required = true)
	private String transmissionTypeName;
	@Schema(description = "ModelYear of the vehicle", example = "2020", required = true)
	private String modelYear;
	@Schema(description = "ModelName of the vehicle", example = "KICKS", required = true)
	private String modelName;
	@Schema(description = "TrimLevelDescription of the vehicle", example = "SR", required = true)
	private String trimLevelDescription;
	@Schema(description = "BodyStyleName of the vehicle", example = "WAGON 4 DOOR", required = true)
	private String bodyStyleName;
	@Schema(description = "InteriorColorName of the vehicle", example = "CHARCOAL", required = true)
	private String interiorColorName;
	@Schema(description = "ExteriorColorName of the vehicle", example = "2TONEORGBLKROOF", required = true)
	private String exteriorColorName;
	@Schema(description = "VehicleImageUrl of the vehicle", 
			example = "https://www.nissanusa.com/content/dam/Nissan/us/assets/2020/kicks/2020-nissan-kicks-21210-exterior-xah.png.ximg.l_6_m.smart.png", required = true)
	private String vehicleImageUrl;
	
	public VinDetailsResponse() {
		
	}

	public VinDetailsResponse(String status, String vin, String drivetrainName, String transmissionTypeName, String modelYear,
			String modelName, String trimLevelDescription, String bodyStyleName, String interiorColorName,
			String exteriorColorName, String vehicleImageUrl) {
		super();
		this.status = status;
		this.vin = vin;
		this.drivetrainName = drivetrainName;
		this.transmissionTypeName = transmissionTypeName;
		this.modelYear = modelYear;
		this.modelName = modelName;
		this.trimLevelDescription = trimLevelDescription;
		this.bodyStyleName = bodyStyleName;
		this.interiorColorName = interiorColorName;
		this.exteriorColorName = exteriorColorName;
		this.vehicleImageUrl = vehicleImageUrl;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getDrivetrainName() {
		return drivetrainName;
	}

	public void setDrivetrainName(String drivetrainName) {
		this.drivetrainName = drivetrainName;
	}

	public String getTransmissionTypeName() {
		return transmissionTypeName;
	}

	public void setTransmissionTypeName(String transmissionTypeName) {
		this.transmissionTypeName = transmissionTypeName;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getTrimLevelDescription() {
		return trimLevelDescription;
	}

	public void setTrimLevelDescription(String trimLevelDescription) {
		this.trimLevelDescription = trimLevelDescription;
	}

	public String getBodyStyleName() {
		return bodyStyleName;
	}

	public void setBodyStyleName(String bodyStyleName) {
		this.bodyStyleName = bodyStyleName;
	}

	public String getInteriorColorName() {
		return interiorColorName;
	}

	public void setInteriorColorName(String interiorColorName) {
		this.interiorColorName = interiorColorName;
	}

	public String getExteriorColorName() {
		return exteriorColorName;
	}

	public void setExteriorColorName(String exteriorColorName) {
		this.exteriorColorName = exteriorColorName;
	}

	public String getVehicleImageUrl() {
		return vehicleImageUrl;
	}

	public void setVehicleImageUrl(String vehicleImageUrl) {
		this.vehicleImageUrl = vehicleImageUrl;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "VinDetailsResponse [status=" + status + ", vin=" + vin + ", drivetrainName=" + drivetrainName
				+ ", transmissionTypeName=" + transmissionTypeName + ", modelYear=" + modelYear + ", modelName="
				+ modelName + ", trimLevelDescription=" + trimLevelDescription + ", bodyStyleName=" + bodyStyleName
				+ ", interiorColorName=" + interiorColorName + ", exteriorColorName=" + exteriorColorName
				+ ", vehicleImageUrl=" + vehicleImageUrl + "]";
	}

}
