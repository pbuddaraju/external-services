package com.nissan.cpo.externalservicelambda.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class JobHistory {
	
	@Schema(description = "Value of the repeatRepairIndicator for the JobHistory", example = "false")
	private String repeatRepairIndicator;
	@Schema(description = "Value of the jobNumberString for the JobHistory", example = "A")
    private String jobNumberString;
	@Schema(description = "Value of the operationName for the JobHistory", example = "CAR WASH")
    private String operationName;
	@Schema(description = "Value of the operationID for the JobHistory", example = "CW")
    private String operationID;
	@Schema(description = "List of ServiceLaborHistory values", example = "")
    private List<ServiceLaborHistory> serviceLaborHistory;
	@Schema(description = "List of servicePartsHistory values", example = "")
    private List<ServicePartsHistory> servicePartsHistory;
	@Schema(description = "Value of the CodeAndComments of the Job History", example = "")
    private CodesAndComments codesAndComments;
    
    public JobHistory() {
    	
    }
    
	public JobHistory(String repeatRepairIndicator, String jobNumberString, String operationName, String operationID,
			List<ServiceLaborHistory> serviceLaborHistory, List<ServicePartsHistory> servicePartsHistory,
			CodesAndComments codesAndComments) {
		super();
		this.repeatRepairIndicator = repeatRepairIndicator;
		this.jobNumberString = jobNumberString;
		this.operationName = operationName;
		this.operationID = operationID;
		this.serviceLaborHistory = serviceLaborHistory;
		this.servicePartsHistory = servicePartsHistory;
		this.codesAndComments = codesAndComments;
	}

	public String getRepeatRepairIndicator ()
    {
        return repeatRepairIndicator;
    }

    public void setRepeatRepairIndicator (String repeatRepairIndicator)
    {
        this.repeatRepairIndicator = repeatRepairIndicator;
    }

    public String getJobNumberString ()
    {
        return jobNumberString;
    }

    public void setJobNumberString (String jobNumberString)
    {
        this.jobNumberString = jobNumberString;
    }

    public String getOperationName ()
    {
        return operationName;
    }

    public void setOperationName (String operationName)
    {
        this.operationName = operationName;
    }

    public String getOperationID ()
    {
        return operationID;
    }

    public void setOperationID (String operationID)
    {
        this.operationID = operationID;
    }

    public List<ServiceLaborHistory> getServiceLaborHistory() {
		return serviceLaborHistory;
	}

	public void setServiceLaborHistory(List<ServiceLaborHistory> serviceLaborHistory) {
		this.serviceLaborHistory = serviceLaborHistory;
	}

	public List<ServicePartsHistory> getServicePartsHistory() {
		return servicePartsHistory;
	}

	public void setServicePartsHistory(List<ServicePartsHistory> servicePartsHistory) {
		this.servicePartsHistory = servicePartsHistory;
	}

	public CodesAndComments getCodesAndComments ()
    {
        return codesAndComments;
    }

    public void setCodesAndComments (CodesAndComments codesAndComments)
    {
        this.codesAndComments = codesAndComments;
    }

	@Override
	public String toString() {
		return "JobHistory [repeatRepairIndicator=" + repeatRepairIndicator + ", jobNumberString=" + jobNumberString
				+ ", operationName=" + operationName + ", operationID=" + operationID + ", serviceLaborHistory="
				+ serviceLaborHistory + ", servicePartsHistory=" + servicePartsHistory + ", codesAndComments="
				+ codesAndComments + "]";
	}

}
