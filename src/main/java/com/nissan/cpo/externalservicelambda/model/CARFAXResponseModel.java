package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class CARFAXResponseModel {
	@Schema(description = "Status of the service", example = "Success", required = true)
	private String status;
	@Schema(description = "Status Message", example = "Pass", required = true)
	private String statusDesc;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
    
}
