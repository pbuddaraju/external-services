package com.nissan.cpo.externalservicelambda.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "GetVehicleServiceHistory")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payload", propOrder = {
    "languageCode",
    "releaseID",
    "systemEnvironmentCode",
    "oagis",
    "star",
    "xsi",
    "schemaLocation"
})
public class GetVehicleServiceHistory {
	
	@XmlAttribute
	private String languageCode;
	
	@XmlAttribute
	private String releaseID;
	
	@XmlAttribute
	private String systemEnvironmentCode;

	@XmlAttribute(name = "xmlns:oagis")
	private String oagis;
	
	@XmlAttribute(name = "xmlns:star")
	private String star;

	@XmlAttribute(name = "xmlns:xsi")
	private String xsi;
	
	@XmlAttribute(name = "xmlns:schemaLocation")
	private String schemaLocation;
	
	public GetVehicleServiceHistory() {
		
	}

	public GetVehicleServiceHistory(String languageCode, String releaseID, String systemEnvironmentCode, String oagis,
			String star, String xsi, String schemaLocation) {
		super();
		this.languageCode = languageCode;
		this.releaseID = releaseID;
		this.systemEnvironmentCode = systemEnvironmentCode;
		this.oagis = oagis;
		this.star = star;
		this.xsi = xsi;
		this.schemaLocation = schemaLocation;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getReleaseID() {
		return releaseID;
	}

	public void setReleaseID(String releaseID) {
		this.releaseID = releaseID;
	}

	public String getSystemEnvironmentCode() {
		return systemEnvironmentCode;
	}

	public void setSystemEnvironmentCode(String systemEnvironmentCode) {
		this.systemEnvironmentCode = systemEnvironmentCode;
	}

	public String getOagis() {
		return oagis;
	}

	public void setOagis(String oagis) {
		this.oagis = oagis;
	}

	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	public String getXsi() {
		return xsi;
	}

	public void setXsi(String xsi) {
		this.xsi = xsi;
	}

	public String getSchemaLocation() {
		return schemaLocation;
	}

	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}

	@Override
	public String toString() {
		return "GetVehicleServiceHistory [languageCode=" + languageCode + ", releaseID=" + releaseID
				+ ", systemEnvironmentCode=" + systemEnvironmentCode + ", oagis=" + oagis + ", star=" + star + ", xsi="
				+ xsi + ", schemaLocation=" + schemaLocation + "]";
	}
	
}
