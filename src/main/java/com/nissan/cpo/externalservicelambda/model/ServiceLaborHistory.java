package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class ServiceLaborHistory {
	@Schema(description = "Value of the laborAdditionalHoursNumeric ServiceLaborHistory", example = "0.0")
	private String laborAdditionalHoursNumeric;
	@Schema(description = "Value of the laborOperationID ServiceLaborHistory", example = "CW")
    private String laborOperationID;
	@Schema(description = "Value of the laborOperationDescription ServiceLaborHistory", example = "")
    private String laborOperationDescription;
    
    public ServiceLaborHistory() {
    	
    }
    
    public ServiceLaborHistory(String laborAdditionalHoursNumeric, String laborOperationID,
			String laborOperationDescription) {
		super();
		this.laborAdditionalHoursNumeric = laborAdditionalHoursNumeric;
		this.laborOperationID = laborOperationID;
		this.laborOperationDescription = laborOperationDescription;
	}

	public String getLaborAdditionalHoursNumeric ()
    {
        return laborAdditionalHoursNumeric;
    }

    public void setLaborAdditionalHoursNumeric (String laborAdditionalHoursNumeric)
    {
        this.laborAdditionalHoursNumeric = laborAdditionalHoursNumeric;
    }

    public String getLaborOperationID ()
    {
        return laborOperationID;
    }

    public void setLaborOperationID (String laborOperationID)
    {
        this.laborOperationID = laborOperationID;
    }

    public String getLaborOperationDescription ()
    {
        return laborOperationDescription;
    }

    public void setLaborOperationDescription (String laborOperationDescription)
    {
        this.laborOperationDescription = laborOperationDescription;
    }

	@Override
	public String toString() {
		return "ServiceLaborHistory [laborAdditionalHoursNumeric=" + laborAdditionalHoursNumeric + ", laborOperationID="
				+ laborOperationID + ", laborOperationDescription=" + laborOperationDescription + "]";
	}

}
