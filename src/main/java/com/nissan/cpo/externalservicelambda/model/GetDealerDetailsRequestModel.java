package com.nissan.cpo.externalservicelambda.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetDealerDetailsRequestModel {
	@Schema(description = "Country code of the app", example = "US", required = true)
	private String countryCode;
	@Schema(description = "Language value of the app", example = "en-us", required = true)
    private String languageCode;
	@Schema(description = "Brand of the app", example = "NI", required = true)
    private String division;
	@Schema(description = "Username of the requester", example = "DWAHIZ39", required = true)
    private String userId;
	@Schema(description = "Usertype of the requester", example = "D", required = true)
    private String userType;
	@Schema(description = "Dealercode of the requester | optional param", example = "5309", required = false)
    private String dealerCode;
	@Schema(description = "Name of the application", example = "CPO", required = true)
    private String applicationName;
	@Schema(description = "AppVersion of the app", example = "1.2.0", required = false)
    private String appVersion;
	@Schema(description = "Device type used by the requester", example = "desktop", required = false)
    private String deviceType;
	@Schema(description = "Environment of the app", example = "prod", required = false)
    private String env;
    
    public GetDealerDetailsRequestModel() {
    	
    }

	public GetDealerDetailsRequestModel(String countryCode, String languageCode, String division, String userId,
			String userType, String dealerCode, String applicationName, String appVersion, String deviceType,
			String env) {
		super();
		this.countryCode = countryCode;
		this.languageCode = languageCode;
		this.division = division;
		this.userId = userId;
		this.userType = userType;
		this.dealerCode = dealerCode;
		this.applicationName = applicationName;
		this.appVersion = appVersion;
		this.deviceType = deviceType;
		this.env = env;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	@Override
	public String toString() {
		return "GetDealerDetailsRequestModel [countryCode=" + countryCode + ", languageCode=" + languageCode
				+ ", division=" + division + ", userId=" + userId + ", userType=" + userType + ", dealerCode="
				+ dealerCode + ", applicationName=" + applicationName + ", appVersion=" + appVersion + ", deviceType="
				+ deviceType + ", env=" + env + "]";
	}
    
}
