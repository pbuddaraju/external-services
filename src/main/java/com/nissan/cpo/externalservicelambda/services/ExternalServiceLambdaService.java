package com.nissan.cpo.externalservicelambda.services;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nissan.cpo.externalservicelambda.common.CPOConstants;
import com.nissan.cpo.externalservicelambda.common.ExternalServiceUtils;
import com.nissan.cpo.externalservicelambda.model.CARFAXResponseModel;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsRequestModel;
import com.nissan.cpo.externalservicelambda.model.GetDealerDetailsResponseModel;

import reactor.core.publisher.Mono;

@Service
public class ExternalServiceLambdaService {
	
	@Autowired
	private WebClient.Builder webclientBuilder;
	
	public GetDealerDetailsResponseModel getDealerDetailsResponse(GetDealerDetailsRequestModel getDealerDetailsRequest) {
		GetDealerDetailsResponseModel getDealerDetailsResponse = new GetDealerDetailsResponseModel();
		JSONObject jsonObj = new JSONObject();
		try {

			String accessToken = ExternalServiceUtils.wso2Oauth(webclientBuilder);
			
			ClientResponse clientResponse = webclientBuilder.build()
					.post()
					.uri(CPOConstants.DEALERSTOCK_USERAUTHORIZATION_URL)
					.headers(httpHeaders -> {
						httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
						httpHeaders.set(CPOConstants.AUTHORIZATION, CPOConstants.BEARER+accessToken);
					})
					.body(Mono.just(getDealerDetailsRequest), GetDealerDetailsRequestModel.class)
					.exchange()
					.block();

			if(clientResponse.statusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
				getDealerDetailsResponse.setStatus(CPOConstants.GNRL_ERR);
			} else if(clientResponse.statusCode() == HttpStatus.OK) {
				jsonObj = convertClientResponseToJSON(clientResponse);
				if(jsonObj != null && jsonObj.getString("status").equalsIgnoreCase(CPOConstants.FAILURE)) {
					if(jsonObj.getString("errorCode").equalsIgnoreCase("Error-632")) {
						getDealerDetailsResponse.setStatus(CPOConstants.NO_ACTIVE_DEALERS);
					} else if(jsonObj.getString("errorCode").equalsIgnoreCase("Error-632")) {
						getDealerDetailsResponse.setStatus(CPOConstants.NO_BRAND_DEALERS);
					} else if(jsonObj.getString("errorCode").equalsIgnoreCase("Error-633")) {
						getDealerDetailsResponse.setStatus(CPOConstants.NO_ACTIVE_OR_BRAND_DEALERS);
					} else if(jsonObj.getString("errorCode").equalsIgnoreCase("Error-620")) {
						getDealerDetailsResponse.setStatus(CPOConstants.GNRL_ERR);
					}
				} else if (jsonObj != null && jsonObj.getString("status").equalsIgnoreCase(CPOConstants.SUCCESS)) {	
					byte[] jsonData = jsonObj.toString().getBytes();
					ObjectMapper mapper = new ObjectMapper();
					getDealerDetailsResponse = mapper.readValue(jsonData, GetDealerDetailsResponseModel.class);
				}
			}
			return getDealerDetailsResponse;
		} catch (Exception e) {
			getDealerDetailsResponse.setStatus(CPOConstants.GNRL_ERR);
			return getDealerDetailsResponse;
		}

	}
	
	public JSONObject convertClientResponseToJSON(ClientResponse clientResponse) {
		JSONObject json = null;
		try {
			String responseString = clientResponse.bodyToMono(String.class).block();
			json = new JSONObject(responseString); 
			return json;
		} catch(Exception e) {
			return json;
		}
	}
	
	public CARFAXResponseModel getCARFAXResponse(String countryCode, String languageCode, String vin) {
		String carfaxResponse = null;
		CARFAXResponseModel response = new CARFAXResponseModel();
		try {
			WebClient webClient = WebClient.create(CPOConstants.CARFAX_URL+vin);
			Mono<String> carfaxMonoRes = webClient.get()
			        .retrieve()
			        .bodyToMono(String.class);	
			carfaxResponse = carfaxMonoRes.block();
			response.setStatus(CPOConstants.SUCCESS);
			response.setStatusDesc(carfaxResponse);
		}catch(Exception e) {
			response.setStatus(CPOConstants.FAILURE);
			response.setStatusDesc(e.getMessage());
		}
		return response;
	}

}
