package com.nissan.cpo.externalservicelambda.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nissan.cpo.externalservicelambda.common.CPOConstants;
import com.nissan.cpo.externalservicelambda.common.CPOErrorConstants;
import com.nissan.cpo.externalservicelambda.common.ErrorResponseModel;
import com.nissan.cpo.externalservicelambda.exception.DataNotFoundException;
import com.nissan.cpo.externalservicelambda.exception.InvalidRequestException;

@ControllerAdvice
public class CPOGlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<?> handleDataNotFoundException(DataNotFoundException exception){

		ErrorResponseModel errorResponseModel = new ErrorResponseModel(CPOConstants.FAILURE, new Date(), exception.getErrorCode(), 
				exception.getErrorMessage(), exception.getRequestUri());

		return new ResponseEntity<>(errorResponseModel, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleGeneralException(Exception exception){

		ErrorResponseModel errorResponseModel = new ErrorResponseModel(CPOConstants.FAILURE, new Date(), CPOErrorConstants.CPO_ERR_CODE_GNRL_ERR, 
				exception.getMessage(), exception.getMessage());

		return new ResponseEntity<>(errorResponseModel, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<?> handleInvalidRequestException(InvalidRequestException exception){

		ErrorResponseModel errorResponseModel = new ErrorResponseModel("Failure", new Date(), exception.getErrorCode(), 
				exception.getErrorMessage(), exception.getRequestUri());

		return new ResponseEntity<>(errorResponseModel, HttpStatus.BAD_REQUEST);
	}

}
