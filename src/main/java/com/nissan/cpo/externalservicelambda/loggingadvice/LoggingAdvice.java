package com.nissan.cpo.externalservicelambda.loggingadvice;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;


@Aspect
@Component
public class LoggingAdvice {
	
	String className = this.getClass().getSimpleName();
	
	Logger log = LoggerFactory.getLogger(LoggingAdvice.class);
	
	@Pointcut(value="execution(* com.nissan.cpo.externalservicelambda.controller.*.*(..))")
	public void cpoExternalPointcut() {
		
	}
	
	@Pointcut(value="execution(* com.nissan.cpo.externalservicelambda.services.*.*(..))")
	public void cpoExternalPointcutServices() {
		
	}
	
	
	@Around("cpoExternalPointcut()")
	public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable {
		    Object obj = new Object();			
			ObjectMapper mapper = new ObjectMapper();
			String methodName = pjp.getSignature().getName();
			String className = pjp.getTarget().getClass().getSimpleName();
			Object[] array = pjp.getArgs();
			log.info("Method invoked1 "+ className + ":"+methodName+"():" + " request: "
			+ mapper.writeValueAsString(array));
			obj = pjp.proceed();
			log.info(className + ":"+methodName+"():" + " response: "
					+ mapper.writeValueAsString(obj));
			return obj;
			
	}
	
	@Around("cpoExternalPointcutServices()")
	public Object applicationLoggerForServices(ProceedingJoinPoint pjp) throws Throwable {
		    Object obj = new Object();			
			ObjectMapper mapper = new ObjectMapper();
			String methodName = pjp.getSignature().getName();
			String className = pjp.getTarget().getClass().getSimpleName();
			Object[] array = pjp.getArgs();
			log.info("Method invoked1 "+ className + ":"+methodName+"():" + " request: "
			+ mapper.writeValueAsString(array));
			obj = pjp.proceed();
			log.info(className + ":"+methodName+"():" + " response: "
					+ mapper.writeValueAsString(obj));
			return obj;
			
	}
	
	
}
