package com.nissan.cpo.externalservicelambda.soapclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapConfig {
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("com.nissan.cpo.externalservicelambda.servicehistorywsdl", "com.nissan.cpo.externalservicelambda.vinlookupservicewsdl");
		return marshaller;
	}
	
}
