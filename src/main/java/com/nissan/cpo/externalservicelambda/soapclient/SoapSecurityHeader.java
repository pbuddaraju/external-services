package com.nissan.cpo.externalservicelambda.soapclient;

import java.io.IOException;
import java.util.Calendar;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerException;

import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.nissan.cpo.externalservicelambda.common.ExternalServiceUtils;

public class SoapSecurityHeader implements WebServiceMessageCallback{
	
	private String usernameDBS;
	private String passwordDBS;
	
	public SoapSecurityHeader() {
		
	}

	public SoapSecurityHeader(String usernameDBS, String passwordDBS) {
		super();
		this.usernameDBS = usernameDBS;
		this.passwordDBS = passwordDBS;
	}
	
	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		try {
			 SOAPMessage soapMessage = ((SaajSoapMessage)message).getSaajMessage();
	         SOAPHeader header = soapMessage.getSOAPHeader();	         	       
	         SOAPPart soapPart = soapMessage.getSOAPPart();
	         SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	         soapEnvelope.addAttribute(soapEnvelope.createName("xmlns:soap"), "http://schemas.xmlsoap.org/soap/envelope/");
	         soapEnvelope.addAttribute(soapEnvelope.createName("xmlns:tran"), "http://www.starstandards.org/webservices/2009/transport"); 
	         SOAPElement payloadManifest = header.addChildElement("payloadManifest", "tran");
	         SOAPElement manifest = payloadManifest.addChildElement("manifest", "tran");
	         manifest.setAttribute("contentID", "1");
	         manifest.setAttribute("element", "GetVehicleServiceHistory");
	         manifest.setAttribute("namespaceURI", "1");
	         manifest.setAttribute("relatedID", "5.4.4");
	         manifest.setAttribute("version", "1");
	         SOAPHeaderElement security = header.addHeaderElement(new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse"));  
	         security.addAttribute(soapEnvelope.createName("soap:mustUnderstand"), "1");
	         security.addAttribute(soapEnvelope.createName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
	         SOAPElement timeStamp = security.addChildElement("Timestamp", "wsu");
	         timeStamp.addAttribute(soapEnvelope.createName("wsu:Id"), "Timestamp-2");
	         SOAPElement timestampCreated = timeStamp.addChildElement("Created", "wsu");
	         Calendar cal1 = Calendar.getInstance();
	         cal1.add(Calendar.DATE, -1);
	         timestampCreated.setTextContent(ExternalServiceUtils.getDateFormat(cal1.getTime()));
	         SOAPElement timestampExpires = timeStamp.addChildElement("Expires", "wsu");
	         Calendar cal = Calendar.getInstance();
	         cal.add(Calendar.DATE, 1);
	         timestampExpires.setTextContent(ExternalServiceUtils.getDateFormat(cal.getTime()));
	         SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
	         usernameToken.addAttribute(soapEnvelope.createName("wsse:Id"), "UsernameToken-4");
	         SOAPElement username = usernameToken.addChildElement("Username", "wsse");
	         username.setTextContent(usernameDBS);
	         SOAPElement password = usernameToken.addChildElement("Password", "wsse");
	         password.addAttribute(soapEnvelope.createName("Type"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");            
	         password.setTextContent(passwordDBS);
	         SOAPElement nonce = usernameToken.addChildElement("Nonce", "wsse");
	         nonce.addAttribute(soapEnvelope.createName("EncodingType"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");            
	         nonce.setTextContent("laNh0c+C0tjNh8KCj+MUbg==");
	         SOAPElement usernameTokenCreated = usernameToken.addChildElement("Created", "wsu");
	         usernameTokenCreated.setTextContent(ExternalServiceUtils.getCSTDateTime());
	         SOAPHeaderElement action = header.addHeaderElement(new QName("http://schemas.xmlsoap.org/ws/2004/08/addressing", "Action", "wsa"));
	         action.setTextContent("http://www.starstandards.org/webservices/2009/transport/operations/ProcessMessage");
	         SOAPHeaderElement to = header.addHeaderElement(new QName("http://schemas.xmlsoap.org/ws/2004/08/addressing", "To", "wsa"));
	         to.setTextContent("https://test.dev.nnadbs/StarInterface/StarService.asmx");
	         			
		} catch (Exception e) {
            throw new IOException("SecurityHeader ::: doWithMessage ::: General Exception",e);
        }
	}

	public String getUsernameDBS() {
		return usernameDBS;
	}

	public void setUsernameDBS(String usernameDBS) {
		this.usernameDBS = usernameDBS;
	}

	public String getPasswordDBS() {
		return passwordDBS;
	}

	public void setPasswordDBS(String passwordDBS) {
		this.passwordDBS = passwordDBS;
	}

}
