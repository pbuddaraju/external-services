package com.nissan.cpo.externalservicelambda.soapclient;

import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import com.nissan.cpo.externalservicelambda.common.ExternalServiceUtils;

public class VinlookupSoapSecurityHeader implements WebServiceMessageCallback{
	
	private String usernameVinlookup;
	private String passwordVinlookup;
	
	Logger log = LoggerFactory.getLogger(VinlookupSoapSecurityHeader.class);
	
	public VinlookupSoapSecurityHeader() {
		
	}
	
	public VinlookupSoapSecurityHeader(String usernameVinlookup, String passwordVinlookup) {
		super();
		this.usernameVinlookup = usernameVinlookup;
		this.passwordVinlookup = passwordVinlookup;
	}

	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		try {
			
			 SOAPMessage soapMessage = ((SaajSoapMessage)message).getSaajMessage();
	         SOAPHeader header = soapMessage.getSOAPHeader();	         	       
	         SOAPPart soapPart = soapMessage.getSOAPPart();
	         SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	         soapEnvelope.addAttribute(soapEnvelope.createName("xmlns:soapenv"), "http://schemas.xmlsoap.org/soap/envelope/");
	         soapEnvelope.addAttribute(soapEnvelope.createName("xmlns:veh"), "http://www.nissanusa.com/VehicleInformation/");
	         soapEnvelope.addAttribute(soapEnvelope.createName("xmlns:xsd"), "http://webservices.vehicleinfo.com/xsd");
	         SOAPHeaderElement security = header.addHeaderElement(new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse"));  
	         security.addAttribute(soapEnvelope.createName("soapenv:mustUnderstand"), "1");
	         security.addAttribute(soapEnvelope.createName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
	         SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
	         usernameToken.addAttribute(soapEnvelope.createName("wsu:Id"), "UsernameToken-2");
	         SOAPElement username = usernameToken.addChildElement("Username", "wsse");
	         username.setTextContent(usernameVinlookup);
	         SOAPElement password = usernameToken.addChildElement("Password", "wsse");
	         password.addAttribute(soapEnvelope.createName("Type"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");            
	         password.setTextContent(passwordVinlookup);
	         SOAPElement usernameTokenCreated = usernameToken.addChildElement("Created", "wsu");
	         usernameTokenCreated.setTextContent(ExternalServiceUtils.getCSTDateTime());
	         
		} catch (Exception e) {
            throw new IOException("SecurityHeader ::: doWithMessage ::: General Exception",e);
        }
		
	}

	public String getUsernameVinlookup() {
		return usernameVinlookup;
	}

	public void setUsernameVinlookup(String usernameVinlookup) {
		this.usernameVinlookup = usernameVinlookup;
	}

	public String getPasswordVinlookup() {
		return passwordVinlookup;
	}

	public void setPasswordVinlookup(String passwordVinlookup) {
		this.passwordVinlookup = passwordVinlookup;
	}

}
