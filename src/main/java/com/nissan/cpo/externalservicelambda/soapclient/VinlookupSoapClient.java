package com.nissan.cpo.externalservicelambda.soapclient;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.nissan.cpo.externalservicelambda.common.CPOConstants;
import com.nissan.cpo.externalservicelambda.common.CPOErrorConstants;
import com.nissan.cpo.externalservicelambda.common.ExternalServiceUtils;
import com.nissan.cpo.externalservicelambda.exception.DataNotFoundException;
import com.nissan.cpo.externalservicelambda.model.VinDetailsResponse;
import com.nissan.cpo.externalservicelambda.vinlookupservicewsdl.FaultData;
import com.nissan.cpo.externalservicelambda.vinlookupservicewsdl.GetVehicleInformation;
import com.nissan.cpo.externalservicelambda.vinlookupservicewsdl.GetVehicleInformationResponse;
import com.nissan.cpo.externalservicelambda.vinlookupservicewsdl.VehicleInformationRequestType;
import com.nissan.cpo.externalservicelambda.vinlookupservicewsdl.VehicleInformationResponseType;

@Service
public class VinlookupSoapClient extends WebServiceGatewaySupport{
	
	@Autowired 
	  private Jaxb2Marshaller marshaller;
	  private WebServiceTemplate template;
	
	  Logger log = LoggerFactory.getLogger(VinlookupSoapClient.class);
	  
	  public VinDetailsResponse getVinlookupDetails(String vinNumber, String brand, String countryCode, String requestUri) {
		  VinDetailsResponse vinDetailsResponse = new VinDetailsResponse();
		  try {

			  GetVehicleInformation getVehicleInformation = new GetVehicleInformation();
			  VehicleInformationRequestType vehicleInformationRequestType = new VehicleInformationRequestType();

			  JAXBElement<String> vin = new JAXBElement<String>(new QName("http://webservices.vehicleinfo.com/xsd", "VIN"), String.class, vinNumber);
			  JAXBElement<String> lookupOpt = new JAXBElement<String>(new QName("http://webservices.vehicleinfo.com/xsd", "lookupOpt"), String.class, "F");
			  vehicleInformationRequestType.setVIN(vin);
			  vehicleInformationRequestType.setLookupOpt(lookupOpt);

			  JAXBElement<VehicleInformationRequestType> regParams = new JAXBElement<VehicleInformationRequestType>
			  (new QName("http://www.nissanusa.com/VehicleInformation/", "regParams"), VehicleInformationRequestType.class, (VehicleInformationRequestType) vehicleInformationRequestType);

			  getVehicleInformation.setRegParams(regParams);

			  template = new WebServiceTemplate(marshaller);

			  GetVehicleInformationResponse getVehicleInformationResponse = (GetVehicleInformationResponse)template.marshalSendAndReceive
					  (CPOConstants.VINLOOKUP_SOAP_END_POINT, getVehicleInformation, 
							  new VinlookupSoapSecurityHeader(CPOConstants.VINLOOKUP_USERNAME, CPOConstants.VINLOOKUP_PASSWORD));

			  vinDetailsResponse = createResponse(getVehicleInformationResponse, brand, countryCode, requestUri);
			  
			  return vinDetailsResponse;

		  } catch (DataNotFoundException e) {
			  log.error("VinlookupSoapClient class - getVinlookupDetails() method - inside DataNotFoundException");
			  throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		  } catch(Exception e) {
			  log.error("VinlookupSoapClient class - getVinlookupDetails() method - inside General Exception" + e.getMessage());
			  vinDetailsResponse.setStatus(CPOConstants.FAILURE);
			  return vinDetailsResponse;
		  }
	  }
	  
	  public VinDetailsResponse createResponse(GetVehicleInformationResponse getVehicleInformationResponse, String brand, String countryCode, String requestUri) throws Exception {
		  VinDetailsResponse vinDetailsResponse = new VinDetailsResponse();

		  try {

			  if(getVehicleInformationResponse != null) {

				  JAXBElement<VehicleInformationResponseType> returnResponse = getVehicleInformationResponse.getReturn();
				  VehicleInformationResponseType vehicleInformationResponseType = returnResponse.getValue();

				  if(vehicleInformationResponseType.getStatus().getValue().equalsIgnoreCase("Failed")) {
					  log.info("Inside Vinlookup failed status");
					  FaultData faultData = vehicleInformationResponseType.getFaultData().getValue();
					  if(faultData != null) {
						  String faultCode = faultData.getFaultCode().getValue();
						  if(faultCode.equalsIgnoreCase("Error-610")) {
							  throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_NO_DATA_AVAILABLE, faultData.getFaultDetail().getValue(), 
									  requestUri);
						  }
					  }
				  } else {

					  vinDetailsResponse.setStatus(CPOConstants.SUCCESS);
					  vinDetailsResponse.setVin(vehicleInformationResponseType.getVIN().getValue());
					  vinDetailsResponse.setModelYear(vehicleInformationResponseType.getModelYear().getValue());
					  vinDetailsResponse.setModelName(vehicleInformationResponseType.getModelName().getValue());
					  vinDetailsResponse.setBodyStyleName(vehicleInformationResponseType.getBodyStyleName().getValue());
					  vinDetailsResponse.setDrivetrainName(vehicleInformationResponseType.getDrivetrainName().getValue());
					  vinDetailsResponse.setExteriorColorName(vehicleInformationResponseType.getExteriorColorName().getValue());
					  vinDetailsResponse.setInteriorColorName(vehicleInformationResponseType.getInteriorColorName().getValue());
					  vinDetailsResponse.setTransmissionTypeName(vehicleInformationResponseType.getTransmissionTypeName().getValue());
					  vinDetailsResponse.setTrimLevelDescription(vehicleInformationResponseType.getTrimLevelDescription().getValue());
					  String exteriorColorId = vehicleInformationResponseType.getExteriorColorId().getValue();
					  String nmacModelCode = vehicleInformationResponseType.getNMCModelCode().getValue();
					  String modelYear = vehicleInformationResponseType.getModelYear().getValue();
					  String modelName = vehicleInformationResponseType.getModelName().getValue();
					  vinDetailsResponse.setVehicleImageUrl(ExternalServiceUtils.buildVehicleImageUrl
							  (brand, countryCode, modelYear, modelName,  nmacModelCode, exteriorColorId));
				  }
			  }

			  return vinDetailsResponse;

		  } catch (DataNotFoundException e) {
			  log.error("VinlookupSoapClient class - createResponse() method - inside DataNotFoundException");
			  throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		  } catch(Exception e) {
			  log.error("VinlookupSoapClient class - createResponse() method - inside General Exception" + e.getMessage());
			  vinDetailsResponse.setStatus(CPOConstants.FAILURE);
			  return vinDetailsResponse;
		  }

	  }

}
