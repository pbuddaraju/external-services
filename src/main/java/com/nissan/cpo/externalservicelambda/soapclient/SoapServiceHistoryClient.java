package com.nissan.cpo.externalservicelambda.soapclient;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.nissan.cpo.externalservicelambda.common.CPOConstants;
import com.nissan.cpo.externalservicelambda.common.CPOErrorConstants;
import com.nissan.cpo.externalservicelambda.exception.DataNotFoundException;
import com.nissan.cpo.externalservicelambda.model.CodesAndComments;
import com.nissan.cpo.externalservicelambda.model.ErrorProcessMessage;
import com.nissan.cpo.externalservicelambda.model.JobHistory;
import com.nissan.cpo.externalservicelambda.model.ServiceLaborHistory;
import com.nissan.cpo.externalservicelambda.model.ServicePartsHistory;
import com.nissan.cpo.externalservicelambda.model.VehicleServiceHistory;
import com.nissan.cpo.externalservicelambda.model.VehicleServiceHistoryDetail;
import com.nissan.cpo.externalservicelambda.servicehistorywsdl.Payload;
import com.nissan.cpo.externalservicelambda.servicehistorywsdl.ProcessContent;
import com.nissan.cpo.externalservicelambda.servicehistorywsdl.ProcessMessage;
import com.nissan.cpo.externalservicelambda.servicehistorywsdl.ProcessMessageResponse;

@Service
public class SoapServiceHistoryClient extends WebServiceGatewaySupport{
	
	  @Autowired 
	  private Jaxb2Marshaller marshaller;
	  private WebServiceTemplate template;
	
	  Logger log = LoggerFactory.getLogger(SoapServiceHistoryClient.class);
	  
	public VehicleServiceHistory getServiceHistory(String countryCode, String division, String vinNumber, String dealerCode) throws Exception {
		VehicleServiceHistory vehicleServiceHistory = new VehicleServiceHistory();
		try {
			
			ProcessMessage processMessageObj = new ProcessMessage();
			processMessageObj = createRequest(countryCode, division, vinNumber, dealerCode);
			
			template = new WebServiceTemplate(marshaller);
			
			ProcessMessageResponse processMessageResponse = (ProcessMessageResponse)template.marshalSendAndReceive(CPOConstants.DBS_SOAP_END_POINT, processMessageObj, 
					new SoapSecurityHeader(CPOConstants.DBS_USERNAME,CPOConstants.DBS_PASSWORD));
			  
			log.info("SoapServiceHistoryClient class - getServiceHistory() method - Content Id: "+processMessageResponse.getPayload().getContent().get(0).getId());
			
			vehicleServiceHistory = serviceHistoryResponse(processMessageResponse);
			    
			return vehicleServiceHistory;

		} catch (DataNotFoundException e) {
			log.error("SoapServiceHistoryClient class - getServiceHistory() method - inside DataNotFoundException" + e.getMessage());
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch(Exception e) {
			log.error("SoapServiceHistoryClient class - getServiceHistory() method - inside General Exception" + e.getMessage());
			vehicleServiceHistory.setStatus(CPOConstants.FAILURE);
			return vehicleServiceHistory;
		}

	}
	
	public ProcessMessage createRequest(String countryCode, String division, String vinNumber, String dealerCode) throws Exception {
		try {
			ProcessMessage processMessageObj = new ProcessMessage();
			Payload payloadObj = new Payload();
			List<ProcessContent> processContentList = new ArrayList<>();
			ProcessContent processContent = new ProcessContent();
			processContent.setId("1");
			
			  Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
				
				Element getServiceHistory = document.createElement("star:GetVehicleServiceHistory");
				document.appendChild(getServiceHistory);
				getServiceHistory.setAttribute("languageCode", "en-US");
				getServiceHistory.setAttribute("releaseID", "5.5.4");
				getServiceHistory.setAttribute("systemEnvironmentCode", "Production");
				getServiceHistory.setAttribute("xmlns:oagis", "http://www.openapplications.org/oagis/9");
				getServiceHistory.setAttribute("xmlns:star", "http://www.starstandard.org/STAR/5");
				getServiceHistory.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
				getServiceHistory.setAttribute("xsi:schemaLocation", "http://www.starstandard.org/STAR/5 /STAR/Rev5.5.4/BODs/Standalone/GetVehicleServiceHistory.xsd");
				
				Element applicationArea = document.createElement("star:ApplicationArea");
				getServiceHistory.appendChild(applicationArea);
				
				Element sender = document.createElement("star:Sender");
				applicationArea.appendChild(sender);
				
				Element creatorNameCode = document.createElement("star:CreatorNameCode");
				sender.appendChild(creatorNameCode);
				creatorNameCode.insertBefore(document.createTextNode(dealerCode), creatorNameCode.getLastChild());
				
				Element senderNameCode = document.createElement("star:SenderNameCode");
				sender.appendChild(senderNameCode);
				senderNameCode.insertBefore(document.createTextNode(dealerCode), senderNameCode.getLastChild());
				
				Element dealerNumberID = document.createElement("star:DealerNumberID");
				sender.appendChild(dealerNumberID);
				dealerNumberID.insertBefore(document.createTextNode(dealerCode), dealerNumberID.getLastChild());
				
				Element dealerCountryCode = document.createElement("star:DealerCountryCode");
				sender.appendChild(dealerCountryCode);
				dealerCountryCode.insertBefore(document.createTextNode(countryCode), dealerCountryCode.getLastChild());

				Element creationDateTime = document.createElement("star:CreationDateTime");
				applicationArea.appendChild(creationDateTime);
				creationDateTime.insertBefore(document.createTextNode("2013-04-04T20:19:20Z"), creationDateTime.getLastChild());
				
				Element bODID = document.createElement("star:BODID");
				applicationArea.appendChild(bODID);
				bODID.insertBefore(document.createTextNode("4121a622-2995-449e-bc06-edbabf148a87"), bODID.getLastChild());
				
				Element destination = document.createElement("star:Destination");
				applicationArea.appendChild(destination);
				
				Element destinationNameCode = document.createElement("star:DestinationNameCode");
				destination.appendChild(destinationNameCode);
				destinationNameCode.insertBefore(document.createTextNode(division), destinationNameCode.getLastChild());
				
				Element getVehicleServiceHistoryRetrievalDataArea = document.createElement("star:GetVehicleServiceHistoryRetrievalDataArea");
				getServiceHistory.appendChild(getVehicleServiceHistoryRetrievalDataArea);
				
				Element get = document.createElement("star:Get");
				getVehicleServiceHistoryRetrievalDataArea.appendChild(get);
				get.setAttribute("maxItems", "59");
				get.setAttribute("recordSetReferenceId", "6ff9bbcb-68de-40cf-b398-049060cdccd2");
				get.setAttribute("recordSetSaveIndicator", "true");
				get.setAttribute("recordSetStartNumber", "1");
				get.setAttribute("uniqueIndicator", "true");
				
				Element expresion = document.createElement("oagis:Expression");
				get.appendChild(expresion);
				
				Element vehicleServiceHistoryRetrieval = document.createElement("star:VehicleServiceHistoryRetrieval");
				getVehicleServiceHistoryRetrievalDataArea.appendChild(vehicleServiceHistoryRetrieval);
				
				Element vehicleServiceHistoryRetrievalHeader = document.createElement("star:VehicleServiceHistoryRetrievalHeader");
				vehicleServiceHistoryRetrieval.appendChild(vehicleServiceHistoryRetrievalHeader);
				
				Element documentDateTime = document.createElement("star:DocumentDateTime");
				vehicleServiceHistoryRetrievalHeader.appendChild(documentDateTime);
				documentDateTime.insertBefore(document.createTextNode("2013-04-04T20:19:20Z"), documentDateTime.getLastChild());
				
				Element documentIdentificationGroup = document.createElement("star:DocumentIdentificationGroup");
				vehicleServiceHistoryRetrievalHeader.appendChild(documentIdentificationGroup);
				
				Element documentIdentification = document.createElement("star:DocumentIdentification");
				documentIdentificationGroup.appendChild(documentIdentification);
				
				Element documentID = document.createElement("star:DocumentID");
				documentIdentification.appendChild(documentID);
				documentID.insertBefore(document.createTextNode("023f4e1d-3e4f-4ad4-869a-160fcbf4f10a"), documentID.getLastChild());
				
				Element vehicleRetrieval = document.createElement("star:VehicleRetrieval");
				vehicleServiceHistoryRetrievalHeader.appendChild(vehicleRetrieval);
				
				Element vin = document.createElement("star:VIN");
				vehicleRetrieval.appendChild(vin);
				vin.insertBefore(document.createTextNode(vinNumber), vin.getLastChild());
			  
			processContent.setAny(document.getDocumentElement());
			processContentList.add(processContent);
			payloadObj.setContent(processContentList);
			
			processMessageObj.setPayload(payloadObj);
			
			return processMessageObj;
			
		} catch (DataNotFoundException e) {
			log.error("SoapServiceHistoryClient class - createRequest() method - inside DataNotFoundException" + e.getMessage());
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch(Exception e) {
			log.info("SoapServiceHistoryClient class - createRequest() method - inside General Exception" + e.getMessage());
			throw new Exception(e.getMessage());
		}
		
	}
	
	public VehicleServiceHistory serviceHistoryResponse(ProcessMessageResponse processMessageResponse) throws Exception {
        VehicleServiceHistory vehicleServiceHistory = new VehicleServiceHistory();
		try {
			
			//Framing VehicleServiceHistory response
            Document documentResp = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            
            JAXBContext context = JAXBContext.newInstance(processMessageResponse.getClass());
            context.createMarshaller().marshal(processMessageResponse, documentResp);
            			
            List<VehicleServiceHistoryDetail> vehicleServiceHistoryDetailList = new ArrayList<VehicleServiceHistoryDetail>();
            
            NodeList nodeList = documentResp.getElementsByTagName("*");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                	
                	if(node.getNodeName().equalsIgnoreCase("ShowVehicleServiceHistory")) {
                		vehicleServiceHistory.setStatus(CPOConstants.SUCCESS);
                	}
                	if(node.getNodeName().equalsIgnoreCase("ns2:BODFailureMessage")) {
                		
                		Element errorElement = (Element) node;
                		NodeList errorNodeList = errorElement.getElementsByTagName("ns2:ErrorProcessMessage");
                		for (int n = 0; n < errorNodeList.getLength(); n++) {
                			Node errorNode = errorNodeList.item(n);
                			if (errorNode.getNodeType() == Node.ELEMENT_NODE) {
                				Element errorChildElement = (Element) errorNode;
                		ErrorProcessMessage errorProcessMessage = new ErrorProcessMessage();
                		errorProcessMessage.setDescription(errorChildElement.getElementsByTagName("ns2:Description")
                				.item(0).getTextContent());
                		errorProcessMessage.setType(errorChildElement.getElementsByTagName("ns2:Type")
                				.item(0).getTextContent());
                		errorProcessMessage.setReasonCode(errorChildElement.getElementsByTagName("ns2:ReasonCode")
                				.item(0).getTextContent());
                		throw new DataNotFoundException(CPOErrorConstants.CPO_ERR_CODE_DBS_VALID_ERR, 
                				errorProcessMessage.getReasonCode(), errorProcessMessage.getDescription());
                			}
                		}
                	}
                	if(node.getNodeName().equalsIgnoreCase("DeliveryDate")) {
                		vehicleServiceHistory.setDeliveryDate(node.getTextContent());
                	} 
                	if(node.getNodeName().equalsIgnoreCase("VehicleBuildDate")) {
                		vehicleServiceHistory.setVehicleBuildDate(node.getTextContent());
                	} 
                	if(node.getNodeName().equalsIgnoreCase("InServiceDate")) {
                		vehicleServiceHistory.setInServiceDate(node.getTextContent());
                	}
                	if(node.getNodeName().equalsIgnoreCase("InServiceDistanceMeasure")) {
                		vehicleServiceHistory.setInServiceDistanceMeasure(node.getTextContent() + " miles");
                	}
                	if(node.getNodeName().equalsIgnoreCase("WarrantyExpirationDate")) {
                		vehicleServiceHistory.setWarrantyExpirationDate(node.getTextContent());                    	
                	}  
                	if(node.getNodeName().equalsIgnoreCase("WarrantyEndDistanceMeasure")) {
                		vehicleServiceHistory.setWarrantyEndDistanceMeasure(node.getTextContent() + " miles");                    	
                	}


                	if(node.getNodeName().equalsIgnoreCase("VehicleServiceHistoryDetail")) {
                		List<JobHistory> jobHistoryList = new ArrayList<JobHistory>();
                		VehicleServiceHistoryDetail vehicleServiceHistoryDetail = new VehicleServiceHistoryDetail();
                		Element element = (Element) node;
                		vehicleServiceHistoryDetail.setRepairOrderOpenedDate(element.getElementsByTagName("RepairOrderOpenedDate")
                				.item(0).getTextContent());
                		vehicleServiceHistoryDetail.setRepairOrderCompletedDate(element.getElementsByTagName("RepairOrderCompletedDate")
                				.item(0).getTextContent());
                		vehicleServiceHistoryDetail.setInDistanceMeasure(element.getElementsByTagName("InDistanceMeasure")
                				.item(0).getTextContent() + " miles");
                		vehicleServiceHistoryDetail.setOutDistanceMeasure(element.getElementsByTagName("OutDistanceMeasure")
                				.item(0).getTextContent() + " miles");

                		NodeList jobHistoryNodeList = element.getElementsByTagName("JobHistory");

                		for (int j = 0; j < jobHistoryNodeList.getLength(); j++) {
                			Node jobHistoryNode = jobHistoryNodeList.item(j);
                			JobHistory jobHistory = new JobHistory();
                			if (jobHistoryNode.getNodeType() == Node.ELEMENT_NODE) {
                				Element jobElement = (Element) jobHistoryNode;
                				jobHistory.setJobNumberString(jobElement.getElementsByTagName("JobNumberString")
                						.item(0).getTextContent());
                				jobHistory.setOperationID(jobElement.getElementsByTagName("OperationID")
                						.item(0).getTextContent());
                				jobHistory.setOperationName(jobElement.getElementsByTagName("OperationName")
                						.item(0).getTextContent());
                				jobHistory.setRepeatRepairIndicator(jobElement.getElementsByTagName("RepeatRepairIndicator")
                						.item(0).getTextContent());

                				NodeList codeCommentsNodeList = jobElement.getElementsByTagName("CodesAndComments");

                				for (int k = 0; k < codeCommentsNodeList.getLength(); k++) {
                					Node codeCommentsNode = codeCommentsNodeList.item(k);
                					if (codeCommentsNode.getNodeType() == Node.ELEMENT_NODE) {
                						Element codeCommentsElement = (Element) codeCommentsNode;
                						CodesAndComments codesAndComments = new CodesAndComments();
                						codesAndComments.setCauseDescription(codeCommentsElement.getElementsByTagName("CauseDescription")
                								.item(0).getTextContent());
                						codesAndComments.setComplaintDescription(codeCommentsElement.getElementsByTagName("ComplaintDescription")
                								.item(0).getTextContent());
                						codesAndComments.setCorrectionDescription(codeCommentsElement.getElementsByTagName("CorrectionDescription")
                								.item(0).getTextContent());
                						codesAndComments.setMiscellaneousNotes(codeCommentsElement.getElementsByTagName("MiscellaneousNotes")
                								.item(0).getTextContent());
                						codesAndComments.setCauseDescription(codeCommentsElement.getElementsByTagName("CauseDescription")
                								.item(0).getTextContent());
                						codesAndComments.setJobDenialDescription(codeCommentsElement.getElementsByTagName("JobDenialDescription")
                								.item(0).getTextContent());
                						jobHistory.setCodesAndComments(codesAndComments);
                					}

                				}


                				NodeList servicePartsHistoryNodeList = jobElement.getElementsByTagName("ServicePartsHistory");
                				List<ServicePartsHistory> servicePartsHistoryList = new ArrayList<ServicePartsHistory>();
                				for (int l = 0; l < servicePartsHistoryNodeList.getLength(); l++) {
                					Node servicePartsHistoryNode = servicePartsHistoryNodeList.item(l);
                					if (servicePartsHistoryNode.getNodeType() == Node.ELEMENT_NODE) {
                						Element servicePartsHistoryElement = (Element) servicePartsHistoryNode;
                						ServicePartsHistory servicePartsHistory = new ServicePartsHistory();
                						servicePartsHistory.setItemID(servicePartsHistoryElement.getElementsByTagName("ItemID")
                								.item(0).getTextContent());
                						servicePartsHistory.setItemIdDescription(servicePartsHistoryElement.getElementsByTagName("ItemIdDescription")
                								.item(0).getTextContent());
                						servicePartsHistory.setItemQuantity(servicePartsHistoryElement.getElementsByTagName("ItemQuantity")
                								.item(0).getTextContent());
                						servicePartsHistoryList.add(servicePartsHistory);
                					}
                					jobHistory.setServicePartsHistory(servicePartsHistoryList);
                				}



                				NodeList serviceLaborHistoryNodeList = jobElement.getElementsByTagName("ServiceLaborHistory");
                				List<ServiceLaborHistory> serviceLaborHistoryList = new ArrayList<ServiceLaborHistory>();
                				for (int l = 0; l < serviceLaborHistoryNodeList.getLength(); l++) {
                					Node serviceLaborHistoryNode = serviceLaborHistoryNodeList.item(l);
                					if (serviceLaborHistoryNode.getNodeType() == Node.ELEMENT_NODE) {
                						Element serviceLaborHistoryElement = (Element) serviceLaborHistoryNode;
                						ServiceLaborHistory serviceLaborHistory = new ServiceLaborHistory();
                						serviceLaborHistory.setLaborOperationID(serviceLaborHistoryElement.getElementsByTagName("LaborOperationID")
                								.item(0).getTextContent());
                						serviceLaborHistory.setLaborOperationDescription(serviceLaborHistoryElement.getElementsByTagName("LaborOperationDescription")
                								.item(0).getTextContent());
                						serviceLaborHistory.setLaborAdditionalHoursNumeric(serviceLaborHistoryElement.getElementsByTagName("LaborAdditionalHoursNumeric")
                								.item(0).getTextContent());
                						serviceLaborHistoryList.add(serviceLaborHistory);
                					}
                					jobHistory.setServiceLaborHistory(serviceLaborHistoryList);
                				}
                			}
                			jobHistoryList.add(jobHistory);

                		}
                		vehicleServiceHistoryDetail.setJobHistory(jobHistoryList);
                		vehicleServiceHistoryDetailList.add(vehicleServiceHistoryDetail);
                	}

                }
                    
            }
            vehicleServiceHistory.setVehicleServiceHistoryDetail(vehicleServiceHistoryDetailList);
					    
			return vehicleServiceHistory;
			
		} catch (DataNotFoundException e) {
			log.error("SoapServiceHistoryClient class - serviceHistoryResponse() method - inside DataNotFoundException" + e.getMessage());
			throw new DataNotFoundException(e.getErrorCode(), e.getErrorMessage(), e.getRequestUri()); 
		} catch(Exception e) {
			log.error("SoapServiceHistoryClient class - serviceHistoryResponse() method - inside General Exception" + e.getMessage());
			throw new Exception(e.getMessage());
		}
		
	}
	
}




