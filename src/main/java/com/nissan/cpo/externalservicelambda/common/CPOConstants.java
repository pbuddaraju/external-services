package com.nissan.cpo.externalservicelambda.common;

public final class CPOConstants {
	
	public static final String GNRL_ERR = "GNRL_ERR";
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";
	public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
	public static final String NO_ACTIVE_DEALERS = "NO_ACTIVE_DEALERS";
	public static final String NO_BRAND_DEALERS = "NO_BRAND_DEALERS";
	public static final String NO_ACTIVE_OR_BRAND_DEALERS = "NO_ACTIVE_OR_BRAND_DEALERS";
	public static final String INVALID_APPLICATION_NAME = "INVALID_APPLICATION_NAME";
	public static final String INVALID_USERID = "INVALID_USERID";
	public static final String INVALID_COUNTRYCODE = "INVALID_COUNTRYCODE";
	public static final String INVALID_LANGUAGECODE = "INVALID_LANGUAGECODE";
	public static final String INVALID_DIVISION = "INVALID_DIVISION";
	public static final String INVALID_USERTYPE = "INVALID_USERTYPE";
	
	//URLs
	public static final String WSO2_OAUTH_URL = "https://stage.services.nissanusa.com/token";
	public static final String WSO2_BASIC_AUTH = "Basic VTRoZnRoZGgyS2oyRWd2Znd2SHVmXzV1dG80YTpFcnVCeGd0bDlhWGs1YmRUcURuTHluOG8ydUlh";
	public static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String GRANT_TYPE = "grant_type";
	public static final String CLIENT_CREDENTIALS = "client_credentials";
	public static final String AUTHORIZATION = "Authorization";
	public static final String DEALERSTOCK_USERAUTHORIZATION_URL = "https://stage.services.nissanusa.com/dealerstock/admin/1.0/getUserAuthorization";
	public static final String BEARER = "Bearer ";

	//CARFAX
	public static final String CARFAX_URL = "http://socket.CARFAX.com:8080/?UID=D38695142&REQUEST=D20&VIN=";
	
	//DBS
    public static final String DBS_SOAP_END_POINT = "https://dbsdms.stage.extranet.nissan-ix.com/StarInterface/StarService.asmx";
    public static final String DBS_USERNAME = "NNA_DMS_172";
    public static final String DBS_PASSWORD = "nissan123";
    
    //Vinlookup
    public static final String VINLOOKUP_SOAP_END_POINT = "http://dmgtws.na.nissan.biz/axis2VinLookUp/services/VehicleInformationServiceImplService";
    public static final String VINLOOKUP_USERNAME = "X987597";
    public static final String VINLOOKUP_PASSWORD = "Ry9Wc$Vr4Bc1LfU";

}
