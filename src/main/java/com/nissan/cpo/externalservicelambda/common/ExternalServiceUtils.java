package com.nissan.cpo.externalservicelambda.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.nissan.cpo.externalservicelambda.model.WSO2TokenResponse;

@Service
public class ExternalServiceUtils {

	private static Logger log = LoggerFactory.getLogger(ExternalServiceUtils.class);

	public static String wso2Oauth(WebClient.Builder webclientBuilder) throws Exception {

		try {

			ClientResponse clientResponse = webclientBuilder.build()
					.post()
					.uri(CPOConstants.WSO2_OAUTH_URL)
					.headers(httpHeaders ->{
						httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
						httpHeaders.set(CPOConstants.AUTHORIZATION, CPOConstants.WSO2_BASIC_AUTH);
					})
					.body(BodyInserters.fromFormData(CPOConstants.GRANT_TYPE, CPOConstants.CLIENT_CREDENTIALS))
					.exchange()
					.block();

			WSO2TokenResponse tokenResponse = clientResponse
					.bodyToMono(WSO2TokenResponse.class)
					.onErrorMap(e -> new Exception(CPOErrorConstants.CPO_ERR_CODE_GNRL_ERR))
					.block();

			return tokenResponse.getAccess_token();

		} catch(Exception e) {
			throw new Exception(e.getMessage(), e); 
		}

	}

	public static String getDateFormat(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" );  
		try{
			if(date!=null){
				String yourformattedDate = sdf.format(date);
				return yourformattedDate;
			}else{
				return sdf.format(new Timestamp(System.currentTimeMillis()));
			}
		}catch(Exception e){
			log.error("Inside Exception in getDateFormat ",e);
			return sdf.format(new Timestamp(System.currentTimeMillis()));
		}

	}
	
	public static String getCSTDateTime() {
        Calendar currentdate = Calendar.getInstance();
        String strdate = null;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        strdate = formatter.format(currentdate.getTime());
        TimeZone obj = TimeZone.getTimeZone("CST");

        formatter.setTimeZone(obj);
        strdate = formatter.format(currentdate.getTime());
        return strdate;
    }
	
	public static String buildVehicleImageUrl(String brand, String countryCode, String modelYear, 
			String modelName, String  nmacModelCode, String exteriorColorId) {

		String vehImageUrl = "";
		String vehBrand = "";

		try {

			if(brand.equalsIgnoreCase("NI")) {
				vehBrand = "Nissan";
			}

			vehImageUrl = "https://www." + vehBrand.toLowerCase() + "usa.com/content/dam/" + vehBrand + "/" + countryCode.toLowerCase() + "/assets/" 
					+ modelYear + "/" + modelName.toLowerCase() + "/" + modelYear + "-" + vehBrand.toLowerCase() + "-" + modelName.toLowerCase() + "-"
					+ nmacModelCode + "-" + "exterior-" + exteriorColorId.toLowerCase() + ".png.ximg.l_6_m.smart.png";

			log.info("Printing vehImageUrl: "+ vehImageUrl);

			return vehImageUrl;

		} catch(Exception e) {
			log.error("ExternalServiceUtils class - buildVehicleImageUrl() method - Exception: "+ e.getMessage());
			return vehImageUrl;
		}
	}

}
