package com.nissan.cpo.externalservicelambda.common;

public final class CPOErrorConstants {

	public static final String CPO_ERR_CODE_GNRL_ERR = "Error-620";	
	public static final String CPO_ERR_CODE_INVALID_APPLICATION_NAME = "Error-630";
	public static final String CPO_ERR_CODE_INVALID_USERID = "Error-631";
	public static final String CPO_ERR_CODE_INVALID_COUNTRYCODE = "Error-632";
	public static final String CPO_ERR_CODE_INVALID_LANGUAGECODE = "Error-633";
	public static final String CPO_ERR_CODE_INVALID_DIVISION = "Error-634";
	public static final String CPO_ERR_CODE_INVALID_USERTYPE = "Error-635";
	public static final String CPO_ERR_CODE_NO_ACTIVE_DEALERS = "Error-636";
	public static final String CPO_ERR_CODE_NO_BRAND_DEALERS = "Error-637";
	public static final String CPO_ERR_CODE_NO_ACTIVE_OR_BRAND_DEALERS = "Error-638";
	public static final String CPO_ERR_CODE_NO_DATA_AVAILABLE = "Error-639";
	public static final String CPO_ERR_CODE_INVALID_VIN = "Error-640";
	public static final String CPO_ERR_CODE_INVALID_CARFAX_IP_ADDRESS = "Error-641";
	public static final String CPO_ERR_CODE_INVALID_DEALERCODE = "Error-642";
	public static final String CPO_ERR_CODE_DBS_VALID_ERR = "Error-643"; 
	
	public static final String CPO_ERR_MESSAGE_GNRL_ERR = "General Error. Please try again later";
	public static final String CPO_ERR_MESSAGE_INVALID_APPLICATION_NAME = "Invalid application name passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_USERID = "Invalid user Id passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_COUNTRYCODE = "Invalid country code passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_LANGUAGECODE = "Invalid language code passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_DIVISION = "Invalid division passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_USERTYPE = "Invalid usertype passed in the request";
	public static final String CPO_ERR_MESSAGE_NO_ACTIVE_DEALERS = "No active dealers found. Please use a different User ID";
	public static final String CPO_ERR_MESSAGE_NO_BRAND_DEALERS = "No matching brand dealers found. Please use a different User ID";
	public static final String CPO_ERR_MESSAGE_NO_ACTIVE_OR_BRAND_DEALERS = "No active or matching brand dealers found. Please use a different User ID";
	public static final String CPO_ERR_MESSAGE_INVALID_DEALERCODE = "Invalid dealer code passed in the request";
	public static final String CPO_ERR_MESSAGE_INVALID_VIN = "Invalid Vin passed in the request";
	
}
