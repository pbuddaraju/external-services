package com.nissan.cpo.externalservicelambda.common;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;

public class ErrorResponseModel {
	
	@Schema(description = "Status of the error", example = "Failure", required = true)
	private String status;
	@Schema(description = "Timestamp of the error occurred", example = "2020-09-08T23:48:28.625+00:00", required = true)
	private Date timeStamp;
	@Schema(description = "Specific error code for the error", example = "Error-635", required = true)
	private String errorCode;
	@Schema(description = "Specific error message for the error", example = "Invalid usertype passed in the request", required = true)
	private String errorMessage;
	@Schema(description = "Request uri path of the service", example = "uri=/cpo/external/getDealerDetails", required = true)
	private String details;
	
	public ErrorResponseModel() {
		
	}

	public ErrorResponseModel(String status, Date timeStamp, String errorCode, String errorMessage, String details) {
		super();
		this.status = status;
		this.timeStamp = timeStamp;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.details = details;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "ErrorResponseModel [status=" + status + ", timeStamp=" + timeStamp + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", details=" + details + "]";
	}
	
}
