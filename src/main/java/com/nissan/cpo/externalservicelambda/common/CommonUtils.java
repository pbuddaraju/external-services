/**
 * 
 */
package com.nissan.cpo.externalservicelambda.common;

/**
 * @author X566325
 * @Info   
 * @Created Aug 24, 2020
 */
public class CommonUtils {
	
	private CommonUtils() {

	}
    
    public static boolean isStringNotNullorNotEmpty(String value) {
    	boolean isNotNullorNotEmpty = false;
	    if (value != null && (!(value.equalsIgnoreCase("") ||(value.equalsIgnoreCase("null"))))) {
	    	isNotNullorNotEmpty = true;
	    }
	    return isNotNullorNotEmpty;
    }
    
}
